//go:generate goversioninfo -icon=images/icon.ico
package main

import "strconv"

var locNames = map[string]string{
	"anti_bug":  "Pesticide Spray",
	"harvester": "Harvester",

	"seed": "Sneed",

	"extr_shop": "Mr Snad's tool shack",
	"buy_shop":  "Silly Bob's Seed Store",
	"sell_shop": "Fishes Tremendous Trades",

	"extr_person": "Mr Snad",
	"buy_person":  "Silly Bob",
	"sell_person": "Fish",
}

var npcData = map[string][]string{
	"extra_enter": {
		"Welcome, we got some trinkets and knick-knacks here which might be of use to you.",
		"Back down the mandem eh? What can I offer you today?",
		"What can I offer you from my eccentric collecion?",
	},
	"extra_buy": {
		"I hope it will serve you good.",
		"I trust you will treat it well.",
		"Treat her right, eh?",
	},

	"buy_enter": {
		"G'day lad, welcome to the best selection of Sneeds in the whole town.",
		"G'day, what can I offer you from my elaborate collection?",
		"Welcome, welcome. Please feel free to browse my stash.",
	},
	"buy_buy": {
		"Very good choice.",
		"Splendid!",
		"Did you say coins? I'll take those off of ya.",
	},

	"sell_enter": {
		"That are some nice plants you got there.",
		"Lovely morning, wouldn't you say? Is there something you want to sell?",
	},
	"sell_buy": {
		"Thank you sir, these will fit perfectly into my collection.",
		"An excellent choice.",
		"Perfect!",
	},
}

var localize = map[string]string{
	"selected_type":  "Selected Type",
	"anti_bug_spray": locNames["anti_bug"],
	"auto_harvester": locNames["harvester"],

	"key_escape": "Escape",

	"pause_quit_game":     "Pause/Quit Game",
	"save_game":           "Save Game",
	"show_story":          "Show Story",
	"show_keybinds":       "Show Keybinds",
	"last_save_ago":       "Last save: %s secs ago",
	"squash_bug":          "Squash Bug",
	"prepare_dirt":        "Prepare Dirt",
	"flatten_ground":      "Restore Grass",
	"plant_sneed":         "Plant " + locNames["seed"],
	"harvest":             "Harvest",
	"dig_hole":            "Dig Hole",
	"action_bucket_water": "Dump into or get water from hole",
	"harv_prep_plant":     "Harvest, Prepare and Plant",
	"select_type":         "Select Type",
	"open_sell_sh":        "Go to " + locNames["sell_shop"],
	"open_buy_sh":         "Go to " + locNames["buy_shop"],
	"open_extra_sh":       "Go to " + locNames["extr_shop"],
	"open_achievements":   "Show your achievements",
	"move_up":             "Move Up",
	"move_down":           "Move Down",
	"move_right":          "Move Right",
	"move_left":           "Move Left",

	"growth":       "Growth",
	"health":       "Health",
	"has_bug":      "has Bug",
	"type":         "Type",
	"sneeds":       locNames["seed"] + "s",
	"no_sneeds":    "You don't have any " + locNames["seed"] + "s",
	"hole":         "Empty Hole",
	"water":        "Water Hole",
	"plant":        "Plant",
	"plants":       "Plants",
	"no_plants":    "You don't have any plants",
	"money":        "Money",
	"your_money":   "Your Money",
	"not_money":    "You don't have enough money!",
	"sign":         "$",
	"you_have":     "You have",
	"no_plant_typ": "You don't have any plants of that type!",
	"no_sell":      "You don't have anything to sell",
	"no_seed_f12":  "You don't have any " + locNames["seed"] + "s at the moment, press Escape to get back to the game",
	"bucket":       "Water-Bucket",
	"bucket_fill":  "full",
	"bucket_empt":  "empty",
	"dry_seed":     "Seed has no water",
	"wat_flow":     "Flow Distance",

	"t_warp_30":     "Time Warp +30s",
	"t_warp_30_l":   "+30 Seconds",
	"t_warp_90":     "Time Warp +90s",
	"t_warp_90_l":   "+90 Seconds",
	"anti_bug_30":   locNames["anti_bug"] + " (30min)",
	"anti_bug_30_l": "+30min " + locNames["anti_bug"],
	"bor_harv":      "Borrow the " + locNames["harvester"] + " for 30min",
	"bor_harv_l":    "+30min " + locNames["harvester"],
	"buck_fill":     "Refill your Water-Bucket",
	"buck_fill_l":   "Refilled your Water-Bucket",

	"selected": "◄",

	"harv_load":    locNames["harvester"] + " Load",
	"harv_idle":    locNames["harvester"] + " is idle",
	"harv_harvest": locNames["harvester"] + " is harvesting",

	"block_info":   "Block Info",
	"inventory":    "Your Backpack",
	"info":         "Info",
	"save":         "Save",
	"story":        "Story",
	"keybinds":     "Keybinds",
	"extr_shop":    locNames["extr_shop"],
	"buy_shop":     locNames["buy_shop"],
	"sell_shop":    locNames["sell_shop"],
	"achievements": "Your Achievements",
	"sele_seed":    "Select " + locNames["seed"] + " type",

	"main_start":  "Start Game",
	"main_quit":   "Quit",
	"main_resume": "Resume Game",

	"crash_rep":  "Creating crash report...",
	"crash_fail": "Failed to create crash report!",
	"crash_loc":  "Steve's garden crash report:",
	"creat_err":  "Creating error report...",
	"fail_err":   "Failed to create error report!",
	"err_loc":    "Steve's garden error report:",

	"save_gam":       "Saving game...",
	"save_prep":      "Preparing",
	"save_init":      "Initializing",
	"save_save":      "Saving",
	"save_load":      "Loading",
	"save_v_inv":     "Validating Inventory",
	"save_v_play":    "Validating Player",
	"save_v_map":     "Validating Game Map",
	"save_v_bmap":    "Validating Bug Map",
	"save_v_info":    "Validating Game Info",
	"save_c_inv":     "Cleaning up Inventory",
	"save_c_map":     "Cleaning up Game Map",
	"save_c_bmap":    "Cleaning up Bug Map",
	"save_c_play":    "Cleaning up Player",
	"save_c_info":    "Cleaning up Game Info",
	"save_compl":     "Loading",
	"fail_sav_first": "Failed to save game",
	"fail_save":      "Failed to save game, trying again...",
	"fail_again":     "Failed again",
	"save_succ":      "Successfully saved game",

	"press_entr": "Press enter to continue...",
	"esc_close":  "ESC to close",
	"exit":       "You exited the game, have a great rest of your day!",

	"plant_a": "Amaranth",
	"plant_b": "Bindweed",
	"plant_c": "Catnip",
	"plant_d": "Chickweed",
	"plant_e": "Quackgrass",
	"plant_f": "Crabgrass",
	"plant_g": "Dandelion",
	"plant_h": "Clover",

	"prep_dirt":  "Prepared Dirt",
	"stag_one":   "Tiny",
	"stag_two":   "Small",
	"stag_three": "Medium",
	"stag_grown": "Fully Grown",
	"bug":        "Bug",
	"type_grass": "Grass",

	"welc_buy":  "Welcome to Silly Bob's Seed Store, Silly Bob will make you shop till you drop.",
	"welc_sell": "Welcome to Fishes Tremendous Trades, you sell, fish buys.",
	"welc_extr": "Welcome to the Mr Snad's tool shack, where you can rent tools for more or less competent fools.",

	"ach_new":         "You unlocked a new achievement!",
	"ach_unlocked":    "- Unlocked -",
	"ach_no_unlocked": "You have no unlocked achievements!",
	"ach_locked":      "- Locked -",
	"ach_no_locked":   "You have unlocked all achievements!",
}

var achievements = map[string][]string{
	AchPlantSeed:    {"Gardening 101", "Plant a seed"},
	AchHarvestPlant: {"Growing like a weed", "Harvest a plant"},
	AchPlantClover:  {"Gotta plant 21 more", "Plant a Clover " + locNames["seed"]},

	AchBuyHarvester: {"Automation all the way", "Rent the harvester"},
	AchBuyBugSpray:  {"Bugs be gone", "Get the bug spray"},
	AchBuyTimeWarp:  {"Time Traveler", "Get any time warp"},

	AchNoSneed:  {"Need for sneed", "Have no " + locNames["seed"] + "s"},
	Ach200Sneed: {"Sneed overload", "Have more than 200 " + locNames["seed"] + "s"},
	Ach1kSneed:  {"Sneed addiction", "Have more than 1,000 " + locNames["seed"] + "s"},

	Ach20Plus:   {"Gimme 20 Dollas", "Have more than 20$"},
	Ach100Plus:  {"Thought IT was high paying", "Have more than 100$"},
	Ach10kPlus:  {"Enough to play something else", "Have more than 10,000$"},
	Ach500kPlus: {"Half way there", "Have more than 500,000$"},
	Ach1MPlus:   {"Millionaire", "Have more than 1,000,000$"},
}

const (
	playerChar    = "X"
	harvesterChar = "@"
	bugCharacter  = "#"

	dirtCharacter   = "░"
	borderCharacter = "░"
	grassCharacter  = " "
	holeCharacter   = " "

	plantFullChar  = "¥"
	plantThreeFull = "I"
	plantTwoChar   = "i"
	plantFreshChar = "."
)

var story = "Steve, your neighbour, noticed you were starting to get stressed out by life and needed a healthy outlet to find peace. He has asked you to check on his garden and you agreed.\n" +
	"Steve is planting weeds, cause he feels bad for the outcast plants and tries to give them a new home.\n" +
	"\n" +
	"\n" +
	"You can walk around the garden by using the arrow keys.\n" +
	"\n" +
	"You can select different types of " + locNames["seed"] + "s in your inventory (press e) and plant them.\n" +
	"To plant " + locNames["seed"] + "s you have to first prepare the dirt (press p) and then plant the " + locNames["seed"] + " (press s).\n" +
	"\n" +
	"The plants need time to grow, their growth status will appear in the block info if you walk on them.\n" +
	"They also need water within " + strconv.Itoa(waterReachDistance) + " Blocks of their position. To place water you dig a hole (press d) and dump the contents of your water bucket in it (press q).\n" +
	"After they are fully grown, you can harvest them (press h). You can sell harvested plants in " + locNames["sell_shop"] + " (press o).\n" +
	"You can buy new " + locNames["seed"] + "s in " + locNames["buy_shop"] + " (press b). Different plants have different sell prices.\n" +
	"\n" +
	"You can quit or pause the game by pressing Escape. Quiting the game will pause the growth of all your plants.\n" +
	"\n" +
	"\n" +
	"Credits:\n" +
	"\n" +
	"Developed by Twoot\n" +
	"Tested by Mr. Snad man\n" +
	"Trailer and CGI Art by fish\n" +
	"\n" +
	"Special thanks also to lolicatgirl, fake_rain and somebois for coming up with beautiful features and helping me find all the bugs.\n"
