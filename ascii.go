package main

/*
 *             _____
 *           /    /|_ _____________________
 *          /    // /|                    /|
 *         (====|/ //      item one      / |
 *          (=====|/      item two      / .|
 *         (====|/                     / /||
 *        /___________________________/ / ||
 *        |  _______________________  ||  ||
 *        | ||                      | ||
 *        | ||                      | ||
 *        | |                       | |
 */
func renderAsciiTable(items []string) []string {
	max := 0
	for _, item := range items {
		if len(item) > max {
			max = len(item)
		}
	}

	for len(items) < 3 {
		items = append(items, "")
	}

	leftPad := "     "

	table := []string{
		padStr("", len(items), " ") + leftPad + "    _____",
		padStr("", len(items), " ") + leftPad + "   /    /|_ _" + padStr("", max, "_") + "____________",
		padStr("", len(items), " ") + leftPad + "  /    // /| " + padStr("", max, " ") + "           /|",
	}

	for i, item := range items {
		rend := ""

		if i == 0 {
			rend = padStr("", len(items)-i, " ") + leftPad + " (====|/ //      " + padStr(item, max, " ") + "      / |"
		} else if i == 1 {
			rend = padStr("", len(items)-i, " ") + leftPad + "   (=====|/      " + padStr(item, max, " ") + "      / .|"
		} else if i == 2 {
			rend = padStr("", len(items)-i, " ") + leftPad + "   (====|/       " + padStr(item, max, " ") + "      / /||"
		} else {
			rend = padStr("", len(items)-i, " ") + leftPad + "   /             " + padStr(item, max, " ") + "      / / " + padStr("", i-3, " ") + "||"
		}

		table = append(table, rend)
	}
	table = append(table, leftPad+"   /________"+padStr("", max, "_")+"___________/ / "+padStr("", len(items)-3, " ")+"||",
		leftPad+"   |  ______"+padStr("", max, "_")+"_________  ||  "+padStr("", len(items)-3, " ")+"||",
		leftPad+"   | ||     "+padStr("", max, " ")+"         | ||",
		leftPad+"   | ||     "+padStr("", max, " ")+"         | ||")

	for i := 0; i < len(items)-3; i++ {
		table = append(table, leftPad+"   | ||     "+padStr("", max, " ")+"         | ||")
	}

	table = append(table, leftPad+"   | |      "+padStr("", max, " ")+"         | |")

	return table
}

func renderAsciiTableCallback(items []string, callback func(line string)) {
	lines := renderAsciiTable(items)
	for _, line := range lines {
		callback(line)
	}
}

func padStr(stri string, l int, padChar string) string {
	str := ""
	for i := 0; i < l-len(stri); i++ {
		str += padChar
	}
	return stri + str
}
