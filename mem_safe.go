package main

import (
	"math"
	"math/rand"
	"strconv"
	"strings"
)

var shiftKey = []int{rand.Intn(10), rand.Intn(10), rand.Intn(10), rand.Intn(10), rand.Intn(10), rand.Intn(10)}

func getShiftPart(index int) int {
	i := math.Mod(float64(index), float64(len(shiftKey)))
	return shiftKey[int(i)]
}

func LockInt(i int) int {
	return int(LockInt64(int64(i)))
}
func UnLockInt(i int) int {
	return int(UnLockInt64(int64(i)))
}

func LockString(i string) string {
	s := strings.Split(i, "")
	r := ""

	for _, st := range s {
		in := int64(st[0])
		lck := LockInt64(in)
		r += strconv.FormatInt(lck, 16) + "|"
	}

	return r
}

func UnLockString(i string) string {
	s := strings.Split(i, "|")
	r := ""

	for _, st := range s {
		if st == "" {
			continue
		}

		in, _ := strconv.ParseInt(st, 16, 64)
		ulc := UnLockInt64(in)
		r += string(rune(ulc))
	}

	return r
}

func LockInt64(i int64) int64 {
	s := strings.Split(strconv.FormatInt(i, 10), "")
	r := ""

	for ix, sh := range s {
		in, _ := strconv.ParseInt(sh, 10, 64)
		shiftPart := getShiftPart(ix)

		shifted := in + int64(shiftPart)
		if shifted >= 10 {
			shifted -= 10
		}

		r += strconv.FormatInt(shifted, 10)
	}

	res, _ := strconv.ParseInt("1"+r, 10, 64)
	return res
}

func UnLockInt64(i int64) int64 {
	s := strings.Split(strconv.FormatInt(i, 10), "")
	r := ""

	for ix, sh := range s {
		if ix == 0 {
			continue
		}
		in, _ := strconv.ParseInt(sh, 10, 64)
		shiftPart := getShiftPart(ix - 1)

		shifted := in - int64(shiftPart)
		if shifted < 0 {
			shifted += 10
		}

		r += strconv.FormatInt(shifted, 10)
	}

	res, _ := strconv.ParseInt(r, 10, 64)
	return res
}
