package main

import (
	"math"
	"strconv"
)

const (
	harvesterStateIdle      = 0
	harvesterStateMoved     = 1
	harvesterStateHarvested = 2
)

type Harvester struct {
	x          int
	y          int
	state      int
	multiplier int
}

var harvester Harvester

func loadHarvesterFromGameInfo() {
	harvester = Harvester{
		x: gameInfo.get(gameHarvesterX),
		y: gameInfo.get(gameHarvesterY),
	}

	if harvester.x == 0 {
		harvester.x = 1
	}
	if harvester.y == 0 {
		harvester.y = 1
	}

	harvester.setState(gameInfo.get(gameHarvesterState))
}
func (h *Harvester) cycle() {
	h.updateMultiplier()

	workDone := false
	for i := 0; i < h.multiplier; i++ {
		if harvester.findPlant() {
			workDone = true
		}
		harvester.harvestPlant()
	}
	if !workDone {
		h.setState(harvesterStateIdle)
	}
}
func (h *Harvester) updateMultiplier() bool {
	plants := 0
	seeds := 0
	gameMapMutex.Lock()
	for _, item := range gameMap {
		if item.Key == plantGrown {
			plants++
		} else if isSeed(item) {
			seeds++
		}
	}
	gameMapMutex.Unlock()
	h.multiplier = int(math.Floor((float64(plants) / float64(seeds+plants)) * 100))

	return plants == 0
}
func (h *Harvester) fmtHarvesterLoad() string {
	if h.state == harvesterStateIdle {
		return localize["harv_load"] + ": 0%"
	}

	h.updateMultiplier()
	return localize["harv_load"] + ": " + strconv.Itoa(h.multiplier) + "%"
}
func (h *Harvester) saveToGameInfo() {
	gameInfo.set(gameHarvesterX, h.x)
	gameInfo.set(gameHarvesterY, h.y)
	gameInfo.set(gameHarvesterState, h.state)
}
func (h *Harvester) setState(state int) {
	h.state = state
	if h.state == harvesterStateIdle {
		h.moveTo(1, 1)
	}
	h.saveToGameInfo()
}
func (h *Harvester) isAlive() bool {
	return gameInfo.has(gameExtraAutoHarvester)
}
func (h *Harvester) isAt(x int, y int) bool {
	return h.isAlive() && h.x == x && h.y == y
}
func (h *Harvester) moveTo(x int, y int) {
	h.x = x
	h.y = y
	h.saveToGameInfo()
}

func (h *Harvester) fmtState() string {
	switch h.state {
	case harvesterStateIdle:
		return localize["harv_idle"]
	case harvesterStateMoved:
		return localize["harv_harvest"]
	case harvesterStateHarvested:
		return localize["harv_harvest"]
	}
	return ""
}

func (h *Harvester) findPlant() bool {
	if !h.isAlive() {
		return false
	}

	if h.state == harvesterStateIdle || h.state == harvesterStateHarvested {
		for point, item := range gameMap {
			if item.Key == plantGrown {
				h.moveTo(point.X, point.Y)
				h.setState(harvesterStateMoved)
				return true
			}
		}
	}
	h.saveToGameInfo()
	return false
}

func (h *Harvester) harvestPlant() {
	if !h.isAlive() {
		return
	}

	if h.state == harvesterStateMoved {
		item := gameMap[NPoint(h.x, h.y)]
		if item.Key == plantGrown {
			gameMap[NPoint(h.x, h.y)] = Item{
				Key:       seedStageOne,
				PlaceTime: 0,
				PlantType: item.PlantType,
			}
			if chance(40) {
				inventory.addSeeds(item.PlantType, 1)
			}
			inventory.addPlants(item.PlantType, 1)
		}
		h.setState(harvesterStateHarvested)
	}
	h.saveToGameInfo()
}
