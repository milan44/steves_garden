package main

import (
	"github.com/inconshreveable/go-update"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

const (
	versionURL = "https://gitlab.com/milan44/steves_garden/-/raw/master/main.go"
	updateURL  = "https://gitlab.com/milan44/steves_garden/-/raw/master/build/StevesGarden.exe"
)

func updateGarden() error {
	resp, err := http.Get(updateURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	err = update.Apply(resp.Body, update.Options{})

	return err
}

func checkVersion() (bool, string) {
	err, vers := getVersion()
	if err != nil {
		return false, "Failed to get version number"
	}

	if version == vers {
		return false, "Your version is up to date"
	}
	return true, ""
}

func getVersion() (error, string) {
	resp, err := http.Get("https://gitlab.com/milan44/steves_garden/-/raw/master/main.go")
	if err != nil {
		return err, ""
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err, ""
	}

	return nil, findVersion(string(b))
}

func findVersion(body string) string {
	re := regexp.MustCompile(`(?m)const version = "(.+)"`)
	matches := re.FindAllString(body, -1)

	if len(matches) != 1 {
		return ""
	}
	return strings.ReplaceAll(strings.ReplaceAll(matches[0], "const version = ", ""), "\"", "")
}
