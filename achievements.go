package main

import (
	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
	"math/rand"
	"strings"
	"time"
)

const (
	AchPlantSeed    = "plant_seed"
	AchHarvestPlant = "harvest_plant"
	AchPlantClover  = "plant_clover"

	AchBuyHarvester = "buy_harvester"
	AchBuyBugSpray  = "buy_bug_spray"
	AchBuyTimeWarp  = "buy_warp"

	AchNoSneed  = "no_sneed"
	Ach200Sneed = "200_sneed"
	Ach1kSneed  = "1k_sneed"

	Ach20Plus   = "20_money"
	Ach100Plus  = "100_money"
	Ach10kPlus  = "10k_money"
	Ach500kPlus = "500k_money"
	Ach1MPlus   = "1m_money"
)

func getAchievementName(key string) []string {
	name, ok := achievements[key]
	if !ok {
		return []string{"UNKNOWN", "Unknown Achievement"}
	}
	return name
}

func hasAchievement(key string) bool {
	return inventory.getAchievement(key) > 0
}
func getAchievement(key string) {
	if !hasAchievement(key) {
		inventory.setAchievement(key)

		newAchievement = key
	}
}

func showAchievements(_ *gocui.Gui, _ *gocui.View) error {
	return popup(func(view *gocui.View, name string, firstTime bool) {
		drawAchievements(view)
	}, "achievements", localize["achievements"])
}

var newAchievement = ""

func drawAchievements(view *gocui.View) {
	rand.Seed(time.Now().UnixNano())
	view.Clear()
	vPrintln(view, "")

	if newAchievement != "" {
		n := getAchievementName(newAchievement)

		PrintInColor(view, localize["ach_new"], color.FgGreen)
		vPrintln(view, "")
		PrintInColor(view, "--> ", color.FgBlue)
		PrintInColor(view, n[0], color.FgCyan)
		PrintInColor(view, " <--", color.FgBlue)
		vPrintln(view, "")
		PrintInColor(view, "\""+n[1]+"\"", color.FgHiGreen)
		vPrintln(view, "")
		vPrintln(view, "")
		vPrintln(view, "")
	}

	PrintInColor(view, localize["ach_unlocked"], color.FgCyan)
	vPrintln(view, "")
	k := make([]string, 0)
	var prev []string = nil
	for key, name := range achievements {
		if hasAchievement(key) {
			if prev == nil {
				prev = name
			} else {
				k = append(k, prev[0])
				k = append(k, name[0])
				k = append(k, "\""+prev[1]+"\"")
				k = append(k, "\""+name[1]+"\"")
				k = append(k, "")
				prev = nil
			}
		}
	}
	if prev != nil {
		k = append(k, prev[0])
		k = append(k, "")
		k = append(k, "\""+prev[1]+"\"")
	}
	if len(k) == 0 {
		vPrintln(view, localize["ach_no_unlocked"])
	} else {
		renderDoubleTable(k, view, func(line string, v *gocui.View) {
			if strings.HasPrefix(line, "\"") {
				PrintInColor(v, line, color.FgHiGreen)
			} else {
				PrintInColor(v, line, color.FgGreen)
			}
		})
	}
	vPrintln(view, "")

	vPrintln(view, "")
	PrintInColor(view, localize["ach_locked"], color.FgCyan)
	vPrintln(view, "")
	k = make([]string, 0)
	prev = nil
	for key, name := range achievements {
		if !hasAchievement(key) {
			if prev == nil {
				prev = name
			} else {
				k = append(k, hideString(prev[0]))
				k = append(k, hideString(name[0]))
				k = append(k, "\""+prev[1]+"\"")
				k = append(k, "\""+name[1]+"\"")
				k = append(k, "")
				prev = nil
			}
		}
	}
	if prev != nil {
		k = append(k, hideString(prev[0]))
		k = append(k, "")
		k = append(k, "\""+prev[1]+"\"")
	}
	if len(k) == 0 {
		vPrintln(view, localize["ach_no_locked"])
	} else {
		renderDoubleTable(k, view, nil)
	}

	newAchievement = ""
}

func checkMoneyAchievements() {
	m := inventory.GetCoins()

	if m >= 1000000 {
		getAchievement(Ach1MPlus)
	}
	if m >= 500000 {
		getAchievement(Ach500kPlus)
	}
	if m >= 10000 {
		getAchievement(Ach10kPlus)
	}
	if m >= 100 {
		getAchievement(Ach100Plus)
	}
	if m >= 20 {
		getAchievement(Ach20Plus)
	}
}

func checkSneedAchievements() {
	c := 0
	for _, co := range inventory.Seeds {
		c += UnLockInt(co)
	}

	if c == 0 {
		getAchievement(AchNoSneed)
	} else {
		if c >= 200 {
			getAchievement(Ach200Sneed)
		}
		if c >= 1000 {
			getAchievement(Ach1kSneed)
		}
	}
}
