package main

import (
	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
	"math/rand"
)

func PrintInColor(view *gocui.View, msg string, colors ...color.Attribute) {
	_, _ = color.New(colors...).Fprint(view, msg)
}

func waterCharacter(flow int64, p Point) (string, color.Attribute) {
	if flow < 0 {
		flow = 0
	} else if flow > waterFlowDistance {
		flow = waterFlowDistance
	}

	s := leftShift("████▓▓▓▒▒▒░░░", waterFlowDistance-flow)
	r := rand.Intn(4)

	return string(getRune(s, r)), color.BgCyan
}

func RDirt(p Point) {
	_, _ = color.New(color.BgYellow, chooseFgColor(p, "dirt")).Fprint(gameView, PlayerOrNot(p, dirtCharacter))
}
func RGrass(p Point) {
	_, _ = color.New(color.BgGreen, chooseFgColor(p, "grass")).Fprint(gameView, PlayerOrNot(p, grassCharacter))
}
func RHole(p Point) {
	_, _ = color.New(color.BgBlack, chooseFgColor(p, "hole")).Fprint(gameView, PlayerOrNot(p, holeCharacter))
}
func RWater(p Point, flow int64) {
	ch, cl := waterCharacter(flow, p)

	_, _ = color.New(cl, chooseFgColor(p, "water")).Fprint(gameView, PlayerOrNot(p, ch))
}
func RSeed(p Point, i Item) {
	c := ""
	if i.Key == seedStageOne {
		c = plantFreshChar
	} else if i.Key == seedStageTwo {
		c = plantTwoChar
	} else if i.Key == seedStageThree {
		c = plantThreeFull
	}

	if hasBug(p) {
		c = bugCharacter
	}

	_, _ = color.New(color.BgYellow, chooseFgColor(p, "seed")).Fprint(gameView, PlayerOrNot(p, c))
}
func RPlant(p Point) {
	c := plantFullChar
	if hasBug(p) {
		c = bugCharacter
	}
	_, _ = color.New(color.BgYellow, chooseFgColor(p, "plant")).Fprint(gameView, PlayerOrNot(p, c))
}
func RBorder() {
	_, _ = color.New(color.FgWhite).Fprint(gameView, borderCharacter)
}
func RNewLine() {
	vPrintln(gameView, "")
}

func chooseFgColor(p Point, typ string) color.Attribute {
	if p == player {
		if typ == "hole" || typ == "water" {
			return color.FgWhite
		}
		return color.FgBlack
	} else if typ == "water" {
		return color.FgBlue
	} else if harvester.isAt(p.X, p.Y) {
		return color.FgBlue
	} else if hasBug(p) {
		return color.FgRed
	} else if typ == "seed" || typ == "plant" || typ == "dirt" {
		return color.FgMagenta
	} else {
		return color.FgBlack
	}
}

func PlayerOrNot(p Point, c string) string {
	if p == player {
		return playerChar
	} else if harvester.isAt(p.X, p.Y) {
		return harvesterChar
	}
	return c
}
