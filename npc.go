package main

import (
	"math/rand"
)

// Actions are "buy" and "enter"
// Locations are "extra", "buy" and "sell"
func npcPhrase(location string, action string) string {
	key := location + "_" + action

	str := npcData[key]

	return str[rand.Intn(len(str))]
}
