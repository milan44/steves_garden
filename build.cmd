@echo off

echo Building Windows...
set GOOS=windows
go generate
del build\win\StevesGarden.exe
go build -o build/win/StevesGarden.exe -gcflags=all='-trimpath="C:/Users/Milan Bartky/steves_garden"' -asmflags=all='-trimpath="C:/Users/Milan Bartky/steves_garden"'

echo Building Linux...
set GOOS=linux
go generate
del build\linux\StevesGarden
go build -o build/linux/StevesGarden -gcflags=all='-trimpath="C:/Users/Milan Bartky/steves_garden"' -asmflags=all='-trimpath="C:/Users/Milan Bartky/steves_garden"'

set GOOS=windows