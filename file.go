package main

import (
	"os"
	"os/user"
	"strings"
)

func initSaveOrLoad() string {
	usr, err := user.Current()
	if err != nil {
		panic(err)
	}
	dir := strings.TrimRight(usr.HomeDir, "\\")
	dir = strings.TrimRight(dir, "/")
	dir += "/steves_garden_data/"

	if !fileExists(dir) {
		_ = os.Mkdir(dir, 0777)
	}

	return dir
}

const rot = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_@|1234567890"
const rotSize = 12

func shift(b []byte) []byte {
	bs := []rune(string(b))
	bn := ""
	for _, by := range bs {
		in := strings.Index(rot, string(by))
		if in > -1 {
			in += rotSize
			if in >= len(rot) {
				in -= len(rot)
			}
			bn += string([]rune(rot)[in])
		} else {
			bn += string(by)
		}
	}
	return []byte(bn)
}
func unshift(b []byte) []byte {
	bs := []rune(string(b))
	bn := ""
	for _, by := range bs {
		in := strings.Index(rot, string(by))
		if in > -1 {
			in -= rotSize
			if in < 0 {
				in += len(rot)
			}
			bn += string([]rune(rot)[in])
		} else {
			bn += string(by)
		}
	}
	return []byte(bn)
}
