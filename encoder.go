package main

import (
	"strconv"
	"strings"
)

func encodeMap(m map[Point]Item) string {
	items := make([]string, 0)
	for point, item := range m {
		items = append(items, pointToString(point)+"@"+encodeItem(item))
	}

	return strings.Join(items, "\n")
}
func decodeMap(str string) map[Point]Item {
	lines := strings.Split(str, "\n")
	m := make(map[Point]Item)

	for _, line := range lines {
		lineSplit := strings.Split(line, "@")
		if len(lineSplit) != 2 {
			continue
		}
		point := stringToPoint(lineSplit[0])
		item := decodeItem(lineSplit[1])

		if item == nil || point == nil {
			continue
		}

		m[*point] = *item
	}

	return m
}

func decodeItem(str string) *Item {
	split := strings.Split(str, "|")
	if len(split) != 3 {
		return nil
	}
	placeTime, err := strconv.ParseInt(split[1], 10, 64)
	if err != nil {
		return nil
	}
	item := Item{
		Key:       split[0],
		PlaceTime: placeTime,
		PlantType: split[2],
	}

	return &item
}

func encodeItem(i Item) string {
	return i.Key + "|" + i2s(i.PlaceTime) + "|" + i.PlantType
}
func EncodeBool(b bool) string {
	if b {
		return "true"
	}
	return "false"
}
func DecodeBool(s string, def bool) bool {
	if s == "true" {
		return true
	} else if s == "false" {
		return false
	}
	return def
}

func UnLockMap(m map[string]int) map[string]int {
	r := make(map[string]int)

	for key, val := range m {
		r[UnLockString(key)] = UnLockInt(val)
	}

	return r
}
func LockMap(m map[string]int) map[string]int {
	r := make(map[string]int)

	for key, val := range m {
		r[LockString(key)] = LockInt(val)
	}

	return r
}
func UnLockMap64(m map[string]int64) map[string]int64 {
	r := make(map[string]int64)

	for key, val := range m {
		r[UnLockString(key)] = UnLockInt64(val)
	}

	return r
}
func LockMap64(m map[string]int64) map[string]int64 {
	r := make(map[string]int64)

	for key, val := range m {
		r[LockString(key)] = LockInt64(val)
	}

	return r
}

func encodeInventory(i Inventory) string {
	lines := []string{
		i.SelectedType + "|" + strconv.Itoa(i.GetCoins()) + "|" + EncodeBool(i.BucketIsFilled),
		encodeSimpleMap(UnLockMap(i.Seeds)),
		encodeSimpleMap(UnLockMap(i.Plants)),
		encodeSimpleMap64(UnLockMap64(i.Achievements)),
	}

	return strings.Join(lines, "\n")
}
func encodeSimpleMap(m map[string]int) string {
	lines := make([]string, 0)
	for key, val := range m {
		lines = append(lines, key+"@"+strconv.Itoa(val))
	}
	str := strings.Join(lines, "|")
	if str == "" {
		return "EMPTY"
	}
	return str
}
func encodeSimpleMap64(m map[string]int64) string {
	lines := make([]string, 0)
	for key, val := range m {
		lines = append(lines, key+"@"+strconv.FormatInt(val, 16))
	}
	str := strings.Join(lines, "|")
	if str == "" {
		return "EMPTY"
	}
	return str
}

func decodeInventory(str string) Inventory {
	lines := strings.Split(str, "\n")

	def := defaultInventory()

	if len(lines) < 1 {
		return def
	}

	i := Inventory{}

	data := strings.Split(lines[0], "|")
	if len(data) >= 2 {
		i.SelectedType = data[0]
		in, err := strconv.ParseInt(data[1], 10, 64)
		if err == nil {
			i.SetCoins(int(in))
		} else {
			i.SetCoins(def.GetCoins())
		}
		if len(data) < 3 {
			i.BucketIsFilled = true
		} else {
			i.BucketIsFilled = DecodeBool(data[2], true)
		}
	} else {
		i.SelectedType = def.SelectedType
		i.SetCoins(def.GetCoins())
	}

	if len(lines) < 2 {
		i.Seeds = make(map[string]int)
	} else {
		i.Seeds = LockMap(decodeSimpleMap(lines[1]))

		if len(lines) < 3 {
			i.Plants = make(map[string]int)
		} else {
			i.Plants = LockMap(decodeSimpleMap(lines[2]))

			if len(lines) < 4 {
				i.Achievements = make(map[string]int64)
			} else {
				i.Achievements = LockMap64(decodeSimpleMap64(lines[3]))
			}
		}
	}

	return i
}
func decodeSimpleMap(str string) map[string]int {
	if str == "EMPTY" {
		return make(map[string]int)
	}

	mp := make(map[string]int)
	ll := strings.Split(str, "|")
	for _, l := range ll {
		temp := strings.Split(l, "@")
		if len(temp) != 2 {
			continue
		}
		in, err := strconv.Atoi(temp[1])

		if err != nil {
			continue
		}
		mp[temp[0]] = in
	}
	return mp
}
func decodeSimpleMap64(str string) map[string]int64 {
	if str == "EMPTY" {
		return make(map[string]int64)
	}

	mp := make(map[string]int64)
	ll := strings.Split(str, "|")
	for _, l := range ll {
		temp := strings.Split(l, "@")
		if len(temp) != 2 {
			continue
		}
		in, err := strconv.ParseInt(temp[1], 16, 64)

		if err != nil {
			continue
		}
		mp[temp[0]] = in
	}
	return mp
}

func i2s(i int64) string {
	return strconv.FormatInt(i, 10)
}
