package main

import (
	"fmt"
	"github.com/dustin/go-humanize"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Item struct {
	Key       string
	PlaceTime int64
	PlantType string
}

type Product struct {
	key    string
	name   string
	action string
	price  int
}

func NProduct(key string, name string, action string, price int) Product {
	return Product{
		key:    key,
		name:   name,
		action: action,
		price:  price,
	}
}

type Inventory struct {
	Seeds          map[string]int
	Plants         map[string]int
	BucketIsFilled bool
	SelectedType   string
	Coins          int
	Achievements   map[string]int64
}

var inventory Inventory

func (i *Inventory) GetCoins() int {
	return UnLockInt(i.Coins)
}
func (i *Inventory) GetFormattedCoins() string {
	return localize["sign"] + humanize.Comma(int64(i.GetCoins()))
}
func (i *Inventory) SetCoins(set int) {
	i.Coins = LockInt(set)
}
func (i *Inventory) AddCoins(add int) {
	i.SetCoins(i.GetCoins() + add)
}
func (i *Inventory) SubCoins(add int) {
	i.SetCoins(i.GetCoins() - add)
}

func (i Inventory) getAchievement(key string) int64 {
	key = LockString(key)
	return UnLockInt64(i.Achievements[key])
}
func (i *Inventory) setAchievement(key string) {
	key = LockString(key)

	i.Achievements[key] = LockInt64(time.Now().Unix())
}

func (i Inventory) getSeedCount(typ string) int {
	typ = LockString(typ)
	return UnLockInt(i.Seeds[typ])
}
func (i Inventory) getPlantCount(typ string) int {
	typ = LockString(typ)
	return UnLockInt(i.Plants[typ])
}
func (i Inventory) hasSeeds(typ string) bool {
	typ = LockString(typ)

	if _, ok := i.Seeds[typ]; ok {
		return UnLockInt(i.Seeds[typ]) > 0
	}
	return false
}
func (i Inventory) hasPlants(typ string) bool {
	typ = LockString(typ)

	if _, ok := i.Plants[typ]; ok {
		return UnLockInt(i.Plants[typ]) > 0
	}
	return false
}
func (i *Inventory) addSeeds(typ string, amount int) {
	typ = LockString(typ)

	if _, ok := i.Seeds[typ]; ok {
		amount += UnLockInt(i.Seeds[typ])
	}

	i.Seeds[typ] = LockInt(amount)

	checkSneedAchievements()
}
func (i *Inventory) removeSeeds(typ string, amount int) {
	typ = LockString(typ)

	t := 0
	if _, ok := i.Seeds[typ]; ok {
		t = UnLockInt(i.Seeds[typ])
	}
	t -= amount

	i.Seeds[typ] = LockInt(t)

	checkSneedAchievements()
}
func (i *Inventory) addPlants(typ string, amount int) {
	typ = LockString(typ)

	if _, ok := i.Plants[typ]; ok {
		amount += UnLockInt(i.Plants[typ])
	}

	i.Plants[typ] = LockInt(amount)
}
func (i *Inventory) removePlants(typ string, amount int) {
	typ = LockString(typ)

	t := 0
	if _, ok := i.Plants[typ]; ok {
		t = UnLockInt(i.Plants[typ])
	}
	t -= amount

	i.Plants[typ] = LockInt(t)
}

func (i Inventory) compare(in Inventory) bool {
	return i.ToString() == in.ToString()
}

func (i *Inventory) ToString() string {
	return strings.Join([]string{
		"Inventory {",
		"    Coins:          " + fmt.Sprint(i.GetCoins()),
		"    BucketIsFilled: " + fmt.Sprint(i.BucketIsFilled),
		"    Seeds:          " + fmt.Sprint(i.Seeds),
		"    Plants:         " + fmt.Sprint(i.Plants),
		"    Achievements:   " + fmt.Sprint(i.Achievements),
		"}",
	}, "\n")
}

func renderInventory() {
	inventoryView.Clear()

	keys := make([]string, 0)
	for k := range inventory.Seeds {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	vPrintln(inventoryView, localize["sneeds"]+":")
	sneedAvailable := false
	for _, typ := range keys {
		count := UnLockInt(inventory.Seeds[typ])
		s := ""
		if UnLockString(typ) == inventory.SelectedType {
			s = " " + localize["selected"]
		}
		if count > 0 && getNameForPlantType(UnLockString(typ)) != "" {
			sneedAvailable = true
			vPrintln(inventoryView, "  "+strconv.Itoa(count)+" X "+getNameForPlantType(UnLockString(typ))+" "+plural("Sneed", count)+s)
		}
	}
	if !sneedAvailable {
		vPrintln(inventoryView, "  "+localize["no_sneeds"])
	}
	vPrintln(inventoryView, "")

	keys = make([]string, 0)
	for k := range inventory.Plants {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	vPrintln(inventoryView, localize["plants"]+":")
	plantAvailable := false
	for _, typ := range keys {
		count := UnLockInt(inventory.Plants[typ])
		if count > 0 && getNameForPlantType(UnLockString(typ)) != "" {
			plantAvailable = true
			vPrintln(inventoryView, "  "+strconv.Itoa(count)+" X "+getNameForPlantType(UnLockString(typ))+" "+plural(localize["plant"], count))
		}
	}
	if !plantAvailable {
		vPrintln(inventoryView, "  "+localize["no_plants"])
	}

	vPrintln(inventoryView, "")
	bucket := localize["bucket_fill"]
	if !inventory.BucketIsFilled {
		bucket = localize["bucket_empt"]
	}
	vPrintln(inventoryView, localize["bucket"]+": "+bucket)
	vPrintln(inventoryView, localize["money"]+": "+inventory.GetFormattedCoins())
}

func defaultInventory() Inventory {
	return Inventory{
		Seeds: map[string]int{
			LockString(defaultPlantType): LockInt(3),
		},
		Plants:         make(map[string]int),
		Achievements:   make(map[string]int64),
		SelectedType:   defaultPlantType,
		BucketIsFilled: true,
	}
}

func plural(str string, count int) string {
	if count > 1 {
		return str + "s"
	}
	return str
}
