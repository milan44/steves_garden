@echo off

echo Building...
set GOOS=windows
go generate
del build\win\StevesGarden.exe
go build -o build/win/StevesGarden.exe -gcflags=all='-trimpath="C:/Users/Milan Bartky/steves_garden"' -asmflags=all='-trimpath="C:/Users/Milan Bartky/steves_garden"'

echo Running...
del out.log
build\win\StevesGarden.exe 2> out.log