package main

import (
	"errors"
	"fmt"
	"github.com/jroimartin/gocui"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const waterReachDistance = 2
const waterFlowDistance = 9

func renderInfo() {
	infoView.Clear()
	vPrintln(infoView, localize["selected_type"]+": "+getNameForPlantType(inventory.SelectedType))
	vPrintln(infoView, "")

	if gameInfo.has(gameExtraBugProtection) {
		vPrintln(infoView, localize["anti_bug_spray"]+": "+gameInfo.fmt(gameExtraBugProtection))
		vPrintln(infoView, "")
	}
	if harvester.isAlive() {
		vPrintln(infoView, localize["auto_harvester"]+": "+gameInfo.fmt(gameExtraAutoHarvester))
		vPrintln(infoView, harvester.fmtState())
		vPrintln(infoView, harvester.fmtHarvesterLoad())
		vPrintln(infoView, "")
	}

	if lastSuccessfulSave != 0 {
		vPrintln(infoView, fmt.Sprintf(localize["last_save_ago"], strconv.FormatInt(time.Now().Unix()-lastSuccessfulSave, 10)))
	}

	vPrintln(infoView, "")
	vPrintln(infoView, localize["key_escape"]+": "+localize["pause_quit_game"])
	vPrintln(infoView, "g: "+localize["save_game"])
	vPrintln(infoView, "t: "+localize["show_keybinds"])
	vPrintln(infoView, "i: "+localize["show_story"])
}

func renderKeybindsTable(view *gocui.View) {
	keybinds := []string{
		localize["key_escape"] + ": " + localize["pause_quit_game"],
		"g: " + localize["save_game"],
		"t: " + localize["show_keybinds"],
		"i: " + localize["show_story"],
		"q: " + localize["open_achievements"],
		"",
		"u: " + localize["squash_bug"],
		"p: " + localize["prepare_dirt"],
		"f: " + localize["flatten_ground"],
		"s: " + localize["plant_sneed"],
		"h: " + localize["harvest"],
		"a: " + localize["harv_prep_plant"],
		"d: " + localize["dig_hole"],
		"w: " + localize["action_bucket_water"],
		"",
		"e: " + localize["select_type"],
		"o: " + localize["open_sell_sh"],
		"b: " + localize["open_buy_sh"],
		"x: " + localize["open_extra_sh"],
		"",
		"↑: " + localize["move_up"],
		"↓: " + localize["move_down"],
		"→: " + localize["move_right"],
		"←: " + localize["move_left"],
	}

	renderDoubleTable(keybinds, view, nil)
}

func renderDoubleTable(slice []string, view *gocui.View, customRenderer func(string, *gocui.View)) {
	lines := make([]string, 0)

	max := 0
	openLine := false
	for _, val := range slice {
		if val == "" {
			openLine = false
		} else {
			if openLine {
				openLine = false
			} else {
				openLine = true
				l := len([]rune(val))
				if l > max {
					max = l
				}
			}
		}
	}
	max += 3
	sprint := "%-" + strconv.Itoa(max) + "s"

	openLine = true
	for _, val := range slice {
		if val == "" {
			vPrintln(view, "")
			openLine = true
		} else {
			val = fmt.Sprintf(sprint, val)

			if customRenderer == nil {
				vPrint(view, val)
			} else {
				customRenderer(val, view)
			}

			if !openLine {
				vPrintln(view, "")
				openLine = true
			} else {
				openLine = false
			}
		}
	}

	for _, line := range lines {
		if customRenderer == nil {
			vPrintln(view, line)
		} else {
			customRenderer(line, view)
		}
	}
}

func renderKeybinds(view *gocui.View) {
	vPrintln(view, "")
	renderKeybindsTable(view)
}

const (
	dirt = "dirt"

	hole         = "hole"
	water        = "water"
	flowingWater = "flow_water"

	seedStageOne   = "seed_one"
	seedStageTwo   = "seed_two"
	seedStageThree = "seed_three"

	plantGrown = "plant"

	bug = "bug"
)

const (
	defaultPlantType = "plant_a"
)

var allPlantTypes = []string{}
var allPlantsPriceMap = map[string]int{}
var allPlantsNameMap = map[string]string{}

func registerPlantType(key string, price int) {
	allPlantTypes = append(allPlantTypes, key)
	allPlantsPriceMap[key] = price
	allPlantsNameMap[key] = localize[key]
}

func loadAllPlants() {
	registerPlantType("plant_a", 1)
	registerPlantType("plant_b", 2)
	registerPlantType("plant_c", 16)
	registerPlantType("plant_d", 4)
	registerPlantType("plant_e", 6)
	registerPlantType("plant_f", 1)
	registerPlantType("plant_g", 3)
	registerPlantType("plant_h", 21)
}

const (
	seedStageTwoPlantTime   = 90
	seedStageThreePlantTime = seedStageTwoPlantTime + 120
	plantGrowFullTime       = seedStageThreePlantTime + 240

	bugKillTime = 60
)

func getPriceForPlantType(typ string) int {
	return allPlantsPriceMap[typ]
}

func getNameForPlantType(typ string) string {
	return allPlantsNameMap[typ]
}

func getNameForItem(item Item) string {
	switch item.Key {
	case dirt:
		return localize["prep_dirt"]
	case seedStageOne:
		return localize["stag_one"] + " "
	case seedStageTwo:
		return localize["stag_two"] + " "
	case seedStageThree:
		return localize["stag_three"] + " "
	case plantGrown:
		return localize["stag_grown"] + " "
	case bug:
		return localize["bug"]
	case hole:
		return localize["hole"]
	case water:
		return localize["water"]
	default:
		return localize["type_grass"]
	}
}
func isAffectedByTimeWarp(item Item) bool {
	switch item.Key {
	case seedStageOne, seedStageTwo, seedStageThree:
		return true
	}
	return false
}
func requiresZeroPlantTime(item Item) bool {
	switch item.Key {
	case dirt, hole:
		return true
	}
	return false
}

func isSeed(i Item) bool {
	return i.Key == seedStageOne || i.Key == seedStageTwo || i.Key == seedStageThree
}
func hasWaterNextTo(point Point) (bool, Item) {
	foundItem := Item{}

	up := point
	up.Y--
	if isSetSoft(up) {
		i := gameMap[up]
		if i.Key == water {
			foundItem = i
		}
	}

	down := point
	down.Y++
	if isSetSoft(down) {
		i := gameMap[down]
		if i.Key == water && i.PlaceTime > foundItem.PlaceTime {
			foundItem = i
		}
	}

	left := point
	left.X--
	if isSetSoft(left) {
		i := gameMap[left]
		if i.Key == water && i.PlaceTime > foundItem.PlaceTime {
			foundItem = i
		}
	}

	right := point
	right.X++
	if isSetSoft(right) {
		i := gameMap[right]
		if i.Key == water && i.PlaceTime > foundItem.PlaceTime {
			foundItem = i
		}
	}

	return foundItem.PlaceTime > 0, foundItem
}
func hasWaterNear(point Point, depth int, dir string) bool {
	depth--

	points := make([]Point, 0)
	dirs := make([]string, 0)

	up := point
	up.Y--
	if dir != "down" {
		points = append(points, up)
		dirs = append(dirs, "up")
	}
	down := point
	down.Y++
	if dir != "up" {
		points = append(points, down)
		dirs = append(dirs, "down")
	}
	left := point
	left.X--
	if dir != "right" {
		points = append(points, left)
		dirs = append(dirs, "left")
	}
	right := point
	right.X++
	if dir != "left" {
		points = append(points, right)
		dirs = append(dirs, "right")
	}

	for index, point := range points {
		if isSetSoft(point) {
			i := gameMap[point]
			if i.Key == water {
				return true
			}
		}
		if depth > 0 && hasWaterNear(point, depth, dirs[index]) {
			return true
		}
	}

	return false
}
func isHole(p Point) bool {
	if isSetSoft(p) {
		i := gameMap[p]
		return i.Key == hole
	}
	return false
}

func Bug() Item {
	return Item{
		Key:       bug,
		PlaceTime: 0,
	}
}

func Seed(stage string) Item {
	return Item{
		Key:       stage,
		PlaceTime: 0,
		PlantType: inventory.SelectedType,
	}
}
func Dirt() Item {
	return Item{
		Key: dirt,
	}
}
func Hole() Item {
	return Item{
		Key: hole,
	}
}
func WaterHole() Item {
	return Item{
		Key:       water,
		PlaceTime: waterFlowDistance,
	}
}

var saving = false
var shouldIgnoreExitHandler = false
var lastSuccessfulSave int64 = 0

func saveGame(dontValidate bool, verbose bool) error {
	if saving {
		return nil
	}

	saving = true

	var wg sync.WaitGroup
	wg.Add(1)

	if verbose {
		fmt.Print(localize["save_gam"] + localize["save_prep"] + "         \r")
	}

	var resErr error = nil

	go func(dontValidate bool, verbose bool, wg *sync.WaitGroup) {
		if !shouldIgnoreExitHandler {
			defer exitHandler()
		}

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_init"] + "         \r")
		}

		gm := gameMap
		bm := bugMap
		ppl := player
		in := inventory
		gi := gameInfo
		dir := initSaveOrLoad()

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_save"] + "         \r")
		}

		inv := encodeInventory(in)
		err := ioutil.WriteFile(dir+"temp_inventory.steve", shift([]byte(inv)), 0777)
		if err != nil {
			resErr = err
			wg.Done()
			return
		}

		gmap := encodeMap(gm)
		err = ioutil.WriteFile(dir+"temp_map.steve", shift([]byte(gmap)), 0777)
		if err != nil {
			resErr = err
			wg.Done()
			return
		}

		bmap := encodeMap(bm)
		err = ioutil.WriteFile(dir+"temp_bug.steve", shift([]byte(bmap)), 0777)
		if err != nil {
			resErr = err
			wg.Done()
			return
		}

		pl := pointToString(ppl)
		err = ioutil.WriteFile(dir+"temp_player.steve", shift([]byte(pl)), 0777)
		if err != nil {
			resErr = err
			wg.Done()
			return
		}

		gin := gameInfo.String()
		err = ioutil.WriteFile(dir+"temp_info.steve", shift([]byte(gin)), 0777)
		if err != nil {
			resErr = err
			wg.Done()
			return
		}

		if !dontValidate {
			if verbose {
				fmt.Print(localize["save_gam"] + localize["save_load"] + "         \r")
			}
			i, g, b, p, ggi := loadGame("temp_")

			if verbose {
				fmt.Print(localize["save_gam"] + localize["save_v_inv"] + "         \r")
			}

			if !in.compare(i) {
				resErr = errors.New("validation: invalid inventory file\n" + in.ToString() + "\n" + i.ToString() +
					"\n--- DUMP ---\n" + debugLoadUnshiftFile("temp_", "inventory") + "\n------------\n")
				wg.Done()
				return
			}

			if verbose {
				fmt.Print(localize["save_gam"] + localize["save_v_play"] + "         \r")
			}

			if ppl.X != p.X || ppl.Y != p.Y {
				resErr = errors.New("validation: invalid player location\n" + fmt.Sprint(ppl) + "\n" + fmt.Sprint(p) +
					"\n--- DUMP ---\n" + debugLoadUnshiftFile("temp_", "player") + "\n------------\n")
				wg.Done()
				return
			}

			if verbose {
				fmt.Print(localize["save_gam"] + localize["save_v_map"] + "         \r")
			}

			if !compareMaps(gm, g) {
				resErr = errors.New("validation: invalid game map file\n" + fmt.Sprint(gm) + "\n" + fmt.Sprint(g) +
					"\n--- DUMP ---\n" + debugLoadUnshiftFile("temp_", "map") + "\n------------\n")
				wg.Done()
				return
			}

			if verbose {
				fmt.Print(localize["save_gam"] + localize["save_v_bmap"] + "         \r")
			}

			if !compareMaps(bm, b) {
				resErr = errors.New("validation: invalid bug map file\n" + fmt.Sprint(bm) + "\n" + fmt.Sprint(b) +
					"\n--- DUMP ---\n" + debugLoadUnshiftFile("temp_", "bug") + "\n------------\n")
				wg.Done()
				return
			}

			if verbose {
				fmt.Print(localize["save_gam"] + localize["save_v_info"] + "         \r")
			}

			if !compareNormalMaps(gi.m, ggi.m) {
				resErr = errors.New("validation: invalid game info file\n" + fmt.Sprint(gi) + "\n" + fmt.Sprint(ggi) +
					"\n--- DUMP ---\n" + debugLoadUnshiftFile("temp_", "info") + "\n------------\n")
				wg.Done()
				return
			}
		}

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_c_inv"] + "                \r")
		}

		_ = os.Remove(dir + "inventory.steve")
		_ = os.Rename(dir+"temp_inventory.steve", dir+"inventory.steve")

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_c_map"] + "               \r")
		}

		_ = os.Remove(dir + "map.steve")
		_ = os.Rename(dir+"temp_map.steve", dir+"map.steve")

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_c_bmap"] + "                \r")
		}

		_ = os.Remove(dir + "bug.steve")
		_ = os.Rename(dir+"temp_bug.steve", dir+"bug.steve")

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_c_play"] + "                 \r")
		}

		_ = os.Remove(dir + "player.steve")
		_ = os.Rename(dir+"temp_player.steve", dir+"player.steve")

		if verbose {
			fmt.Print(localize["save_gam"] + localize["save_c_info"] + "              \r")
		}

		_ = os.Remove(dir + "info.steve")
		_ = os.Rename(dir+"temp_info.steve", dir+"info.steve")

		wg.Done()
	}(dontValidate, verbose, &wg)

	wg.Wait()

	if verbose {
		fmt.Print(localize["save_gam"] + localize["save_compl"] + "                    \r")
	}

	saving = false
	if resErr == nil {
		lastSuccessfulSave = time.Now().Unix()
	}
	return resErr
}

func debugLoadUnshiftFile(prefix, file string) string {
	dir := initSaveOrLoad()

	if fileExists(dir + prefix + file + ".steve") {
		d, err := ioutil.ReadFile(dir + prefix + file + ".steve")
		if err != nil {
			return "ERROR-READING"
		} else {
			ud := unshift(d)
			return string(ud)
		}
	}
	return "NOT-EXIST"
}

func loadGame(prefix string) (Inventory, map[Point]Item, map[Point]Item, Point, GameInfo) {
	dir := initSaveOrLoad()

	var inv Inventory
	if fileExists(dir + prefix + "inventory.steve") {
		d, err := ioutil.ReadFile(dir + prefix + "inventory.steve")
		if err != nil {
			inv = defaultInventory()
		} else {
			ud := unshift(d)
			inv = decodeInventory(string(ud))
		}
	} else {
		inv = defaultInventory()
	}

	var mp map[Point]Item
	if fileExists(dir + prefix + "map.steve") {
		d, err := ioutil.ReadFile(dir + prefix + "map.steve")
		if err != nil {
			mp = make(map[Point]Item)
		} else {
			ud := unshift(d)
			mp = decodeMap(string(ud))
		}
	} else {
		mp = make(map[Point]Item)
	}

	var bp map[Point]Item
	if fileExists(dir + prefix + "bug.steve") {
		d, err := ioutil.ReadFile(dir + prefix + "bug.steve")
		if err != nil {
			bp = make(map[Point]Item)
		} else {
			ud := unshift(d)
			bp = decodeMap(string(ud))
		}
	} else {
		bp = make(map[Point]Item)
	}

	var pl Point
	if fileExists(dir + prefix + "player.steve") {
		d, err := ioutil.ReadFile(dir + prefix + "player.steve")
		if err != nil {
			pl = NPoint(10, 10)
		} else {
			ud := unshift(d)
			p := stringToPoint(string(ud))
			if p == nil {
				pl = NPoint(10, 10)
			} else {
				pl = *p
			}
		}
	} else {
		pl = NPoint(10, 10)
	}

	var gi GameInfo
	if fileExists(dir + prefix + "info.steve") {
		d, err := ioutil.ReadFile(dir + prefix + "info.steve")
		if err != nil {
			gi = newGameInfo()
		} else {
			ud := unshift(d)
			gi = gameInfoFromString(string(ud))
		}
	} else {
		gi = newGameInfo()
	}

	return inv, mp, bp, pl, gi
}

func fileExists(file string) bool {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		return false
	}
	return true
}

func pointToString(point Point) string {
	return strconv.Itoa(point.X) + "," + strconv.Itoa(point.Y)
}
func stringToPoint(str string) *Point {
	split := strings.Split(str, ",")

	if len(split) != 2 {
		return nil
	}

	x, err := strconv.Atoi(split[0])
	if err != nil {
		return nil
	}

	y, err := strconv.Atoi(split[1])
	if err != nil {
		return nil
	}

	p := NPoint(x, y)
	return &p
}
