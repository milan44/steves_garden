package main

import (
	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
	"github.com/nsf/termbox-go"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

var player Point

func initPlayerKeys() {
	if err := gui.SetKeybinding("", 'g', gocui.ModNone, savePopup); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 't', gocui.ModNone, showInfo); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'i', gocui.ModNone, showStory); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyEsc, gocui.ModNone, pauseGame); err != nil {
		log.Panicln(err)
	}

	if err := gui.SetKeybinding("", gocui.KeyArrowDown, gocui.ModNone, moveDown); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyArrowLeft, gocui.ModNone, moveLeft); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyArrowRight, gocui.ModNone, moveRight); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyArrowUp, gocui.ModNone, moveUp); err != nil {
		log.Panicln(err)
	}

	if err := gui.SetKeybinding("", 's', gocui.ModNone, placeSeed); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'p', gocui.ModNone, prepareDirt); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'f', gocui.ModNone, flattenGround); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'h', gocui.ModNone, harvest); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'u', gocui.ModNone, squashBug); err != nil {
		log.Panicln(err)
	}

	if err := gui.SetKeybinding("", 'a', gocui.ModNone, prepareAndPlant); err != nil {
		log.Panicln(err)
	}

	if err := gui.SetKeybinding("", 'e', gocui.ModNone, showSelection); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'q', gocui.ModNone, showAchievements); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'o', gocui.ModNone, openSellShop); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'b', gocui.ModNone, openBuyShop); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'x', gocui.ModNone, openExtraShop); err != nil {
		log.Panicln(err)
	}

	if err := gui.SetKeybinding("", 'd', gocui.ModNone, digHole); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", 'w', gocui.ModNone, waterBucketAction); err != nil {
		log.Panicln(err)
	}
}

func pauseGame(g *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		if currentOpen != "" {
			_ = closePopup(currentOpen)
		}
		return nil
	}

	paused = true
	gameIsStarted = false

	g.SetManagerFunc(mainmenuLayout)

	mainMenuOptions[0] = localize["main_resume"]

	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	mainmenu()

	return nil
}

func savePopup(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	paused = true
	return popup(func(view *gocui.View, name string, firstTime bool) {
		view.Clear()

		vPrintln(view, localize["save_gam"])

		err := saveGame(false, false)
		if err != nil {
			vPrintln(view, localize["fail_sav_first"]+" ("+err.Error()+")")
		} else {
			vPrintln(view, localize["save_succ"])
		}
	}, "save_popup", localize["save"])
}

func showInfo(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	return popup(func(view *gocui.View, name string, firstTime bool) {
		view.Clear()

		renderKeybinds(view)
	}, "info_popup", localize["keybinds"])
}

func showStory(gui *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	return popup(func(view *gocui.View, name string, firstTime bool) {
		maxX, _ := gui.Size()
		view.Clear()

		story = strings.ReplaceAll(story, "\n", " NEWLINE ")

		words := strings.Split(story, " ")
		newLines := make([]string, 0)
		for _, word := range words {
			if len(newLines) == 0 {
				newLines = append(newLines, word)
			} else {
				line := newLines[len(newLines)-1] + " " + word
				if word == "NEWLINE" {
					newLines = append(newLines, "")
					continue
				}
				if len(line) > maxX-5 {
					newLines = append(newLines, word)
				} else {
					newLines[len(newLines)-1] = strings.TrimSpace(line)
				}
			}
		}

		vPrintln(view, strings.Join(newLines, "\n"))
	}, "story_popup", localize["story"])
}

func openExtraShop(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	return popup(func(view *gocui.View, name string, firstTime bool) {
		drawExtraShop(view, name, "", firstTime)
	}, "e_shop", localize["extr_shop"])
}
func drawExtraShop(view *gocui.View, name string, err string, setKeys bool) {
	view.Clear()

	vPrintln(view, "   ")
	vPrintln(view, "     "+localize["welc_extr"])
	vPrintln(view, "     "+localize["your_money"]+": "+strconv.Itoa(inventory.GetCoins())+localize["sign"])
	vPrintln(view, "")
	vPrintln(view, "")

	action := "buy"
	if err == "" || err == localize["not_money"] {
		action = "enter"
	}

	vPrint(view, "     ")
	PrintInColor(view, locNames["extr_person"], color.FgCyan)
	vPrint(view, ": ")
	PrintInColor(view, "\""+npcPhrase("extra", action)+"\"\n", color.FgYellow)
	vPrintln(view, "")

	var extraTypes = []Product{
		NProduct("warp_30", localize["t_warp_30"], localize["t_warp_30_l"], 8),
		NProduct("warp_90", localize["t_warp_90"], localize["t_warp_90_l"], 18),
		NProduct("bug_30", localize["anti_bug_30"], localize["anti_bug_30_l"], 58),
		NProduct("auto_30", localize["bor_harv"], localize["bor_harv_l"], 104),
		NProduct("bucket", localize["buck_fill"], localize["buck_fill_l"], 0),
	}

	items := make([]string, 0)

	for i, product := range extraTypes {
		items = append(items, "F"+strconv.Itoa(i+1)+": "+product.name+" ("+strconv.Itoa(product.price)+localize["sign"]+")")

		if setKeys {
			setPopupKey(name, int2key(i+1, false), i, func(index int, view *gocui.View) {
				safelyBuyOrSell()
				product := extraTypes[index]

				if inventory.GetCoins() >= product.price {
					inventory.SubCoins(product.price)

					handleExtraBuy(product.key)

					drawExtraShop(view, name, "-"+strconv.Itoa(product.price)+localize["sign"]+", "+product.action, false)

					checkMoneyAchievements()
				} else {
					drawExtraShop(view, name, localize["not_money"], false)
				}
				finishBuyOrSell()
			})
		}
	}

	renderAsciiTableCallback(items, func(line string) {
		vPrintln(view, line)
	})

	if strings.HasPrefix(err, "-") {
		PrintInColor(view, "     "+err, color.FgRed)
	} else {
		PrintInColor(view, "     "+err, color.FgYellow)
	}
}
func handleExtraBuy(extra string) {
	switch extra {
	case "warp_30":
		go timeWarp(30)
	case "warp_90":
		go timeWarp(90)
	case "bug_30":
		gbp := gameInfo.get(gameExtraBugProtection)
		gameInfo.set(gameExtraBugProtection, gbp+(30*60))

		getAchievement(AchBuyBugSpray)
	case "auto_30":
		gap := gameInfo.get(gameExtraAutoHarvester)
		gameInfo.set(gameExtraAutoHarvester, gap+(30*60))
		if gap == 0 {
			harvester.setState(harvesterStateIdle)
		}

		getAchievement(AchBuyHarvester)
	case "bucket":
		inventory.BucketIsFilled = true
	}
}
func timeWarp(sec int64) {
	gameMapMutex.Lock()
	for point, item := range gameMap {
		if isAffectedByTimeWarp(item) {
			item.PlaceTime += sec
			gameMap[point] = item
		} else if requiresZeroPlantTime(item) {
			item.PlaceTime = 0
			gameMap[point] = item
		}
	}
	gameMapMutex.Unlock()

	getAchievement(AchBuyTimeWarp)
}

func openBuyShop(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	return popup(func(view *gocui.View, name string, firstTime bool) {
		drawBuyShop(view, name, "", firstTime)
	}, "b_shop", localize["buy_shop"])
}
func drawBuyShop(view *gocui.View, name string, err string, setKeys bool) {
	view.Clear()

	vPrintln(view, "   ")
	vPrintln(view, "     "+localize["welc_buy"])
	vPrintln(view, "     "+localize["your_money"]+": "+inventory.GetFormattedCoins())
	vPrintln(view, "")

	action := "buy"
	if err == "" || err == localize["not_money"] {
		action = "enter"
	}

	vPrint(view, "     ")
	PrintInColor(view, locNames["buy_person"], color.FgCyan)
	vPrint(view, ": ")
	PrintInColor(view, "\""+npcPhrase("buy", action)+"\"\n", color.FgYellow)
	vPrintln(view, "")

	items := make([]string, 0)

	for i, typ := range allPlantTypes {
		count := 0
		if inventory.hasSeeds(typ) {
			count = inventory.getSeedCount(typ)
		}
		items = append(items, "F"+strconv.Itoa(i+1)+": "+getNameForPlantType(typ)+" "+localize["sneeds"]+" ("+strconv.Itoa(getPriceForPlantType(typ)*2)+localize["sign"]+") - "+localize["you_have"]+" "+strconv.Itoa(count))

		if setKeys {
			setPopupKey(name, int2key(i+1, false), i, func(index int, view *gocui.View) {
				safelyBuyOrSell()
				typ := allPlantTypes[index]
				pr := getPriceForPlantType(typ) * 2
				if inventory.GetCoins() >= pr {
					inventory.SubCoins(pr)

					inventory.addSeeds(typ, 1)

					drawBuyShop(view, name, "-"+strconv.Itoa(pr)+localize["sign"], false)

					checkMoneyAchievements()
				} else {
					drawBuyShop(view, name, localize["not_money"], false)
				}
				finishBuyOrSell()
			})
		}
	}

	renderAsciiTableCallback(items, func(line string) {
		vPrintln(view, line)
	})

	if strings.HasPrefix(err, "-") {
		PrintInColor(view, "     "+err, color.FgRed)
	} else {
		PrintInColor(view, "     "+err, color.FgYellow)
	}
}

func openSellShop(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	return popup(func(view *gocui.View, name string, firstTime bool) {
		drawSellShop(view, name, "", firstTime)
	}, "s_shop", localize["sell_shop"])
}
func drawSellShop(view *gocui.View, name string, err string, setKeys bool) {
	view.Clear()

	vPrintln(view, "   ")
	vPrintln(view, "     "+localize["welc_sell"])
	vPrintln(view, "     "+localize["your_money"]+": "+inventory.GetFormattedCoins())
	vPrintln(view, "")

	action := "buy"
	if err == "" || err == localize["no_plant_typ"] {
		action = "enter"
	}

	vPrint(view, "     ")
	PrintInColor(view, locNames["sell_person"], color.FgCyan)
	vPrint(view, ": ")
	PrintInColor(view, "\""+npcPhrase("sell", action)+"\"\n", color.FgYellow)
	vPrintln(view, "")

	items := make([]string, 0)

	hasPlants := false
	for i, typ := range allPlantTypes {
		if inventory.hasPlants(typ) {
			hasPlants = true
			items = append(items, "F"+strconv.Itoa(i+1)+": "+getNameForPlantType(typ)+" ("+strconv.Itoa(getPriceForPlantType(typ))+localize["sign"]+") - "+localize["you_have"]+" "+strconv.Itoa(inventory.getPlantCount(typ))+" ["+int2keydesc(i+1)+" sell all]")
		}

		if setKeys {
			setPopupKey(name, int2key(i+1, false), i, func(index int, view *gocui.View) {
				safelyBuyOrSell()
				typ := allPlantTypes[index]
				if inventory.hasPlants(typ) {
					pr := getPriceForPlantType(typ)
					inventory.AddCoins(pr)

					inventory.removePlants(typ, 1)

					drawSellShop(view, name, "+"+strconv.Itoa(pr)+"$", false)

					checkMoneyAchievements()
				} else {
					drawSellShop(view, name, localize["no_plant_typ"], false)
				}
				finishBuyOrSell()
			})
			setPopupKey(name, int2key(i+1, true), i, func(index int, view *gocui.View) {
				safelyBuyOrSell()
				typ := allPlantTypes[index]
				if inventory.hasPlants(typ) {
					cnt := inventory.getPlantCount(typ)
					pr := getPriceForPlantType(typ) * cnt
					inventory.AddCoins(pr)

					inventory.removePlants(typ, cnt)

					drawSellShop(view, name, "+"+strconv.Itoa(pr)+"$ (Sold "+strconv.Itoa(cnt)+" "+getNameForPlantType(typ)+")", false)
				} else {
					drawSellShop(view, name, localize["no_plant_typ"], false)
				}
				finishBuyOrSell()
			})
		}
	}

	if !hasPlants {
		vPrintln(view, "     "+localize["no_sell"])
	} else {
		renderAsciiTableCallback(items, func(line string) {
			vPrintln(view, line)
		})
	}

	if strings.HasPrefix(err, "+") {
		PrintInColor(view, "     "+err, color.FgGreen)
	} else {
		PrintInColor(view, "     "+err, color.FgYellow)
	}
}

func showSelection(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen {
		return nil
	}
	return popup(func(view *gocui.View, name string, firstTime bool) {
		view.Clear()

		hasPlants := false
		for i, typ := range allPlantTypes {
			if inventory.hasSeeds(typ) {
				hasPlants = true
				vPrintln(view, "F"+strconv.Itoa(i+1)+": "+getNameForPlantType(typ))
			}

			if firstTime {
				setPopupKey(name, int2key(i+1, false), i, func(index int, view *gocui.View) {
					safelyBuyOrSell()
					if inventory.hasSeeds(allPlantTypes[index]) {
						inventory.SelectedType = allPlantTypes[index]

						_ = closePopup(name)
					}
					finishBuyOrSell()
				})
			}
		}

		if !hasPlants {
			vPrintln(view, localize["no_seed_f12"])
		}
	}, "selection", localize["sele_seed"])
}

func int2key(i int, secondary bool) gocui.Key {
	switch i {
	case 1:
		if secondary {
			return gocui.Key(termbox.KeyCtrlW)
		}
		return gocui.KeyF1
	case 2:
		if secondary {
			return gocui.Key(termbox.KeyCtrlE)
		}
		return gocui.KeyF2
	case 3:
		if secondary {
			return gocui.Key(termbox.KeyCtrlR)
		}
		return gocui.KeyF3
	case 4:
		if secondary {
			return gocui.Key(termbox.KeyCtrlT)
		}
		return gocui.KeyF4
	case 5:
		if secondary {
			return gocui.Key(termbox.KeyCtrlZ)
		}
		return gocui.KeyF5
	case 6:
		if secondary {
			return gocui.Key(termbox.KeyCtrlU)
		}
		return gocui.KeyF6
	case 7:
		if secondary {
			return gocui.Key(termbox.KeyCtrlI)
		}
		return gocui.KeyF7
	case 8:
		if secondary {
			return gocui.Key(termbox.KeyCtrlO)
		}
		return gocui.KeyF8
	case 9:
		if secondary {
			return gocui.Key(termbox.KeyCtrlP)
		}
		return gocui.KeyF9
	case 10:
		if secondary {
			return gocui.Key(termbox.KeyCtrlF)
		}
		return gocui.KeyF10
	case 11:
		if secondary {
			return gocui.Key(termbox.KeyCtrlG)
		}
		return gocui.KeyF11
	case 12:
		if secondary {
			return gocui.Key(termbox.KeyCtrlH)
		}
		return gocui.KeyF12
	default:
		panic("no key defined for " + strconv.Itoa(i))
	}
}

func int2keydesc(i int) string {
	switch i {
	case 1:
		return "Ctrl+W"
	case 2:
		return "Ctrl+E"
	case 3:
		return "Ctrl+R"
	case 4:
		return "Ctrl+T"
	case 5:
		return "Ctrl+Z"
	case 6:
		return "Ctrl+U"
	case 7:
		return "Ctrl+I"
	case 8:
		return "Ctrl+O"
	case 9:
		return "Ctrl+P"
	case 10:
		return "Ctrl+F"
	case 11:
		return "Ctrl+G"
	case 12:
		return "Ctrl+H"
	default:
		panic("no key defined for " + strconv.Itoa(i))
	}
}

var buySellAction = false

func safelyBuyOrSell() {
	for isDrawingNow() || buySellAction {
		time.Sleep(10 * time.Millisecond)
	}
	buySellAction = true
}
func finishBuyOrSell() {
	buySellAction = false
}

func digHole(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	canBeDug := false
	if isSet(player) {
		item := gameMap[player]
		if item.Key == dirt {
			canBeDug = true
		}
	} else {
		canBeDug = true
	}

	if canBeDug {
		gameMap[player] = Hole()
	}

	renderGame(false)

	return nil
}
func waterBucketAction(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	if isSet(player) {
		item := gameMap[player]
		if item.Key == water {
			gameMap[player] = Hole()
			inventory.BucketIsFilled = true
		} else if item.Key == hole && inventory.BucketIsFilled {
			gameMap[player] = WaterHole()
			inventory.BucketIsFilled = false
		}
	}

	renderGame(false)

	return nil
}
func placeSeed(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	placeSeedAt(player, "")

	renderGame(false)

	return nil
}
func placeSeedAt(p Point, plantType string) {
	if plantType == "" {
		plantType = inventory.SelectedType
	}

	canBePlaced := false
	if isSet(p) {
		item := gameMap[p]
		if item.Key == dirt {
			canBePlaced = true
		}
	}

	if canBePlaced && inventory.hasSeeds(plantType) {
		gameMap[p] = Seed(seedStageOne)
		inventory.removeSeeds(plantType, 1)

		getAchievement(AchPlantSeed)

		if inventory.SelectedType == "plant_h" { // Is Clover
			getAchievement(AchPlantClover)
		}
	}
}
func squashBug(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	if hasBug(player) {
		delete(bugMap, player)
	}

	renderGame(false)

	return nil
}
func harvest(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	harvestAt(player)

	renderGame(false)

	return nil
}
func harvestAt(p Point) {
	canHarvest := false
	if isSet(p) {
		item := gameMap[p]
		if item.Key == plantGrown {
			canHarvest = true
		}
	}

	if canHarvest {
		item := gameMap[p]
		gameMap[p] = Dirt()
		inventory.addSeeds(item.PlantType, rand.Intn(2)+1)
		inventory.addPlants(item.PlantType, 1)

		getAchievement(AchHarvestPlant)
	}
}
func prepareDirtAt(p Point) {
	if isSet(p) {
		item := gameMap[p]
		if isSeed(item) || item.Key == plantGrown {
			inventory.addSeeds(item.PlantType, 1)
		}
	} else if chance(2) {
		inventory.addSeeds(defaultPlantType, 1)
	}

	gameMap[p] = Dirt()
}
func prepareDirt(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	prepareDirtAt(player)

	renderGame(false)

	return nil
}
func prepareAndPlant(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	if isSet(player) {
		item := gameMap[player]
		if item.Key == plantGrown {
			inventory.SelectedType = item.PlantType
			harvestAt(player)
		}
	}
	if gameMap[player].Key == dirt || !isSet(player) {
		prepareDirtAt(player)
	}
	placeSeedAt(player, "")

	renderGame(false)

	return nil
}
func flattenGround(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	if isSet(player) {
		item := gameMap[player]
		if isSeed(item) || item.Key == plantGrown {
			inventory.addSeeds(item.PlantType, 1)
		} else if item.Key == dirt {
			if chance(1) {
				inventory.addSeeds(defaultPlantType, 1)
			}
		}
	}

	delete(gameMap, player)

	renderGame(false)

	return nil
}

func correctPlayerBounds() {
	width, height := getGameSize()
	if player.X < 1 {
		player.X = width - 2
	}
	if player.X > width-2 {
		player.X = 1
	}
	if player.Y < 1 {
		player.Y = height - 2
	}
	if player.Y > height-2 {
		player.Y = 1
	}

	renderGame(false)
}

func moveDown(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	player.Y++
	correctPlayerBounds()
	return nil
}
func moveUp(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	player.Y--
	correctPlayerBounds()
	return nil
}
func moveLeft(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	player.X--
	correctPlayerBounds()
	return nil
}
func moveRight(_ *gocui.Gui, _ *gocui.View) error {
	if isPopupOpen || isDrawingNow() {
		return nil
	}

	player.X++
	correctPlayerBounds()
	return nil
}
