package main

import (
	"math"
	"strconv"
	"strings"
	"sync"
)

const (
	gameExtraBugProtection = "bug_protection"
	gameExtraAutoHarvester = "auto_harvester"
	gameHarvesterState     = "harvester_state"
	gameHarvesterX         = "harvester_x"
	gameHarvesterY         = "harvester_y"
)

type GameInfo struct {
	m   map[string]int
	mtx sync.Mutex
}

var gameInfo GameInfo

func (g *GameInfo) minus(key string) {
	g.mtx.Lock()
	val := g.m[key]
	if val > 0 {
		val--
		g.m[key] = val
	}
	g.mtx.Unlock()
}
func (g *GameInfo) plus(key string) {
	g.mtx.Lock()
	val := g.m[key]
	if val > 0 {
		val++
		g.m[key] = val
	}
	g.mtx.Unlock()
}

func (g *GameInfo) get(key string) int {
	g.mtx.Lock()
	v := g.m[key]
	g.mtx.Unlock()
	return v
}
func (g *GameInfo) has(key string) bool {
	return g.get(key) > 0
}

func (g *GameInfo) set(key string, val int) {
	g.mtx.Lock()
	g.m[key] = val
	g.mtx.Unlock()
}
func (g *GameInfo) fmt(key string) string {
	sec := g.get(key)
	min := int(math.Floor(float64(sec) / 60.0))
	sec -= min * 60

	hou := int(math.Floor(float64(min) / 60.0))
	min -= hou * 60

	str := ""
	if hou > 0 {
		str += strconv.Itoa(hou) + "h "
	}
	if min > 0 {
		str += strconv.Itoa(min) + "m "
	}
	str += strconv.Itoa(sec) + "s"

	return str
}

func (g GameInfo) String() string {
	var str []string

	for key, val := range g.m {
		str = append(str, key+"|"+strconv.Itoa(val))
	}

	if len(str) == 0 {
		return "EMPTY"
	}
	return strings.Join(str, "@")
}

func newGameInfo() GameInfo {
	return GameInfo{m: map[string]int{}, mtx: sync.Mutex{}}
}

func gameInfoFromString(str string) GameInfo {
	g := newGameInfo()
	if str == "EMPTY" {
		return g
	}

	items := strings.Split(str, "@")
	for _, item := range items {
		temp := strings.Split(item, "|")
		if len(temp) != 2 {
			continue
		}
		in, err := strconv.Atoi(temp[1])
		if err != nil {
			continue
		}
		g.m[temp[0]] = in
	}

	return g
}
