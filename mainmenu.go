package main

import (
	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
	"log"
	"math"
)

var selectedRow = 0
var maxMainMenuRows = 2
var mainMenuOptions = []string{
	localize["main_start"],
	localize["main_quit"],
}

var mainMenuView *gocui.View

func mainmenu() {
	if err := gui.SetKeybinding("", gocui.KeyEnter, gocui.ModNone, func(g *gocui.Gui, v *gocui.View) error {
		if selectedRow == 0 {
			startGame()
		} else if selectedRow == 1 {
			return quit(g, v)
		}
		return nil
	}); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyArrowUp, gocui.ModNone, func(_ *gocui.Gui, _ *gocui.View) error {
		selectedRow--
		if selectedRow < 0 {
			selectedRow = maxMainMenuRows - 1
		}
		renderMainMenu()
		return nil
	}); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyArrowDown, gocui.ModNone, func(_ *gocui.Gui, _ *gocui.View) error {
		selectedRow++
		if selectedRow >= maxMainMenuRows {
			selectedRow = 0
		}
		renderMainMenu()
		return nil
	}); err != nil {
		log.Panicln(err)
	}
}

func renderMainMenu() {
	mainMenuView.Clear()

	maxX, maxY := gui.Size()
	renderTitle(mainMenuView, maxY, len(mainMenuOptions))

	for i, opt := range mainMenuOptions {
		paddingLeft := pad(fl(maxX, 2) - fl(strlen(opt), 2))

		vPrint(mainMenuView, paddingLeft)
		if i == selectedRow {
			_, _ = color.New(color.BgWhite, color.FgBlack).Fprintln(mainMenuView, opt)
		} else {
			_, _ = color.New(color.FgWhite).Fprintln(mainMenuView, opt)
		}
	}
}

func mainmenuLayout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	if v, err := g.SetView("mainmenu", -1, -1, maxX, maxY); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		mainMenuView = v

		renderMainMenu()
	}

	return nil
}
func fl(a int, b int) int {
	return int(math.Floor(float64(a) / float64(b)))
}

var gameIsStarted = false
var gameLoopHasBeenStarted = false

func startGame() {
	if gameIsStarted {
		return
	}
	gameIsStarted = true
	paused = false

	gui.SetManagerFunc(layout)

	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	initPlayerKeys()

	if !gameLoopHasBeenStarted {
		gameLoopHasBeenStarted = true
		go gameLoop()
	}

	return
}
