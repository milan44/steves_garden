package main

import (
	"math"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

type Point struct {
	X int `json:"x"`
	Y int `json:"y"`
}

var gameMap map[Point]Item
var gameMapMutex sync.Mutex

var bugMap map[Point]Item
var bugMapMutex sync.Mutex

var isDrawing = 0
var isTicking = false

func isDrawingNow() bool {
	return isDrawing > 0
}
func renderGame(ignoreTick bool) {
	if isDrawingNow() || (isTicking && !ignoreTick) {
		return
	}
	isDrawing++

	renderInventory()

	gameView.Clear()

	width, height := getGameSize()

	for y := 0; y < height; y++ {
		RBorder()
		for x := 1; x < width-1; x++ {
			if y == 0 || y == height-1 {
				RBorder()
				continue
			}

			p := NPoint(x, y)
			if isSet(p) {
				gameMapMutex.Lock()
				item := gameMap[p]
				gameMapMutex.Unlock()

				if isSeed(item) {
					RSeed(p, item)
				} else if item.Key == plantGrown {
					RPlant(p)
				} else if item.Key == dirt {
					RDirt(p)
				} else if item.Key == hole {
					RHole(p)
				} else if item.Key == water {
					RWater(p, item.PlaceTime)
				}
			} else {
				RGrass(p)
			}
		}
		RBorder()
		RNewLine()
	}

	typ := localize["type_grass"]
	additional := ""
	if isSet(player) {
		gameMapMutex.Lock()
		item := gameMap[player]
		gameMapMutex.Unlock()
		typ = getNameForItem(item) + getNameForPlantType(item.PlantType)

		if isSeed(item) {
			growth := int(math.Floor((float64(item.PlaceTime) / float64(plantGrowFullTime)) * 100))
			additional = localize["growth"] + ": " + strconv.Itoa(growth) + "%"
			if hasBug(player) {
				bugMapMutex.Lock()
				bug := bugMap[player]
				bugMapMutex.Unlock()

				health := int(math.Floor(((float64(bugKillTime) - float64(bug.PlaceTime)) / bugKillTime) * 100))
				additional += "\n" + localize["health"] + ": " + strconv.Itoa(health) + "%"
			} else if !hasWaterNear(player, waterReachDistance, "") {
				additional += "\n" + localize["dry_seed"]
			}
		} else if item.Key == plantGrown {
			additional = localize["growth"] + ": 100%"
		} else if item.Key == water {
			additional = localize["wat_flow"] + ": " + strconv.FormatInt(item.PlaceTime, 10)
		}
	}
	if hasBug(player) {
		typ += " " + localize["has_bug"]
	}
	blockInfoView.Clear()
	vPrintln(blockInfoView, localize["type"]+": "+typ)
	if additional != "" {
		vPrintln(blockInfoView, additional)
	}

	renderInfo()

	isDrawing--

	if !isPopupOpen && newAchievement != "" {
		_ = showAchievements(nil, nil)
	}
}

var lastSave int64 = 0

func tickGame() {
	changed := false

	harvester.cycle()

	gameMapMutex.Lock()

	negativePlaceTimes := make([]Point, 0)

	for point, item := range gameMap {
		changed = true
		if isSeed(item) && hasWaterNear(point, waterReachDistance, "") {
			item.PlaceTime++
		}

		if item.PlaceTime < 0 {
			negativePlaceTimes = append(negativePlaceTimes, point)
		}

		if isSeed(item) {
			if item.Key == seedStageOne && item.PlaceTime > seedStageTwoPlantTime {
				item.Key = seedStageTwo
			} else if item.Key == seedStageTwo && item.PlaceTime > seedStageThreePlantTime {
				item.Key = seedStageThree
			} else if item.Key == seedStageThree && item.PlaceTime > plantGrowFullTime {
				item.Key = plantGrown
			}
		} else if item.Key == hole && item.PlaceTime >= 0 {
			has, it := hasWaterNextTo(point)
			if has && it.PlaceTime-1 > 0 {
				item = it
				item.PlaceTime--
			}
		} else if item.Key == water {
			if item.PlaceTime >= waterFlowDistance {
				item.PlaceTime = waterFlowDistance
			} else {
				has, found := hasWaterNextTo(point)
				if !has {
					item.PlaceTime -= 2

					if item.PlaceTime <= 0 {
						item = Hole()
						item.PlaceTime = -1
					}
				} else {
					item.PlaceTime = found.PlaceTime - 1
				}
			}
		}

		if isSeed(item) && getNameForPlantType(item.PlantType) == "" {
			delete(gameMap, point)
			continue
		} else if item.Key == plantGrown && getNameForPlantType(item.PlantType) == "" {
			delete(gameMap, point)
			continue
		}

		gameMap[point] = item

		if !hasBug(point) && (isSeed(item) || item.Key == plantGrown) && hasWaterNear(point, waterReachDistance, "") {
			if (rand.Float64()*100) < 0.1 && !gameInfo.has(gameExtraBugProtection) {
				bugMap[point] = Bug()
			}
		} else if hasBug(point) && (isSeed(item) || item.Key == plantGrown) {
			bugMapMutex.Lock()
			bug := bugMap[point]
			bugMapMutex.Unlock()

			if bug.PlaceTime > bugKillTime {
				gameMap[point] = Dirt()

				bugMapMutex.Lock()
				delete(bugMap, point)
				bugMapMutex.Unlock()
			}
		}
	}

	for _, point := range negativePlaceTimes {
		i := gameMap[point]
		i.PlaceTime++
		gameMap[point] = i
	}

	harvester.findPlant()

	gameMapMutex.Unlock()
	bugMapMutex.Lock()
	for point, bug := range bugMap {
		changed = true

		bug.PlaceTime++
		bugMap[point] = bug

		if !isSet(point) {
			delete(bugMap, point)
		} else {
			gameMapMutex.Lock()
			if !(isSeed(gameMap[point]) || gameMap[point].Key == plantGrown) {
				delete(bugMap, point)
			} else if isSeed(gameMap[point]) {
				i := gameMap[point]
				i.PlaceTime--
				if i.PlaceTime < 0 {
					i.PlaceTime = 0
				}
				gameMap[point] = i
			}
			gameMapMutex.Unlock()
		}
	}
	bugMapMutex.Unlock()
	if changed {
		t := time.Now().Unix()
		if t-lastSave >= 30 {
			go func() {
				_ = saveGame(false, false)
			}()
			lastSave = t
		}
	}

	gameInfo.minus(gameExtraBugProtection)
	gameInfo.minus(gameExtraAutoHarvester)
}

func NPoint(x int, y int) Point {
	return Point{
		X: x,
		Y: y,
	}
}
func isSet(p Point) bool {
	gameMapMutex.Lock()
	s := isSetSoft(p)
	gameMapMutex.Unlock()
	return s
}
func isSetSoft(p Point) bool {
	if _, ok := gameMap[p]; ok {
		return true
	}
	return false
}
func hasBug(p Point) bool {
	bugMapMutex.Lock()
	if _, ok := bugMap[p]; ok {
		bugMapMutex.Unlock()
		return true
	}
	bugMapMutex.Unlock()
	return false
}

func compareMaps(g1 map[Point]Item, g2 map[Point]Item) bool {
	for key, val := range g1 {
		if val != g2[key] {
			return false
		}
	}

	return true
}

func compareNormalMaps(g1 map[string]int, g2 map[string]int) bool {
	for key, val := range g1 {
		if val != g2[key] {
			return false
		}
	}

	return true
}
