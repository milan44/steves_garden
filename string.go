package main

import "math/rand"

func leftShift(s string, i int64) string {
	for x := int64(0); x < i; x++ {
		s = string([]rune(s)[1:]) + string([]rune(s)[:1])
	}
	return s
}

func getRune(s string, i int) rune {
	r := []rune(s)
	if i < 0 {
		i = 0
	} else if i >= len(r) {
		i = len(r) - 1
	}

	return r[i]
}

func hideString(s string) string {
	replace := "█▓▒░"

	newString := ""
	for range []rune(s) {
		newString += string(getRune(replace, rand.Intn(4)))
	}

	return newString
}
