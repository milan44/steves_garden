package main

import (
	"fmt"
	"github.com/gobuffalo/packr"
	"github.com/hajimehoshi/ebiten/audio"
	"github.com/hajimehoshi/ebiten/audio/mp3"
	"github.com/pkg/errors"
	"math/rand"
)

type SoundAsset struct {
	stream audio.Player
	bytes  []byte
}

func NewSoundAsset(bytes []byte) SoundAsset {
	s := SoundAsset{
		bytes: bytes,
	}
	return s
}
func (s *SoundAsset) loadSound() error {
	sound, err := mp3.Decode(audioContext, audio.BytesReadSeekCloser(s.bytes))
	if err != nil {
		return err
	}
	pl, err := audio.NewPlayer(audioContext, sound)
	if err != nil {
		return err
	}
	if pl == nil {
		return errors.New("player is nil")
	}
	s.stream = *pl
	return nil
}

var audioContext *audio.Context

const (
	buySellSound = "buy-sell"
	flattenSound = "flatten"
	harvestSound = "harvest"
	plantSound   = "plant"
	prepareSound = "prepare"
	walkSound    = "walk"
)

var (
	buySellAsset []SoundAsset
	flattenAsset []SoundAsset
	harvestAsset []SoundAsset
	plantAsset   []SoundAsset
	prepareAsset []SoundAsset
	walkAsset    []SoundAsset
)

func loadAssets() {
	c, err := audio.NewContext(32000)
	if err != nil {
		panic(err)
	}
	audioContext = c

	box := packr.NewBox("./assets")

	buySellAsset, err = createSoundAsset([]string{
		"buy-sell.mp3",
		"buy-sell-1.mp3",
	}, box)
	if err != nil {
		panic(err)
	}

	flattenAsset, err = createSoundAsset([]string{
		"flatten-ground.mp3",
		"flatten-ground-1.mp3",
	}, box)
	if err != nil {
		panic(err)
	}

	harvestAsset, err = createSoundAsset([]string{
		"harvest.mp3",
	}, box)
	if err != nil {
		panic(err)
	}

	plantAsset, err = createSoundAsset([]string{
		"plant.mp3",
	}, box)
	if err != nil {
		panic(err)
	}

	prepareAsset, err = createSoundAsset([]string{
		"prepare-dirt.mp3",
		"prepare-dirt-1.mp3",
	}, box)
	if err != nil {
		panic(err)
	}

	walkAsset, err = createSoundAsset([]string{
		"walk.mp3",
		"walk-1.mp3",
		"walk-2.mp3",
		"walk-3.mp3",
		"walk-4.mp3",
		"walk-5.mp3",
		"walk-6.mp3",
		"walk-7.mp3",
		"walk-8.mp3",
		"walk-9.mp3",
	}, box)
	if err != nil {
		panic(err)
	}
}

func playSound(key string) {
	var ar []SoundAsset

	switch key {
	case buySellSound:
		ar = buySellAsset
	case flattenSound:
		ar = flattenAsset
	case harvestSound:
		ar = harvestAsset
	case plantSound:
		ar = plantAsset
	case prepareSound:
		ar = prepareAsset
	case walkSound:
		ar = walkAsset
	}
	asset := ar[rand.Intn(len(ar))]

	fmt.Println(key)

	panicIf(asset.stream.Rewind())
	asset.stream.SetVolume(1)
	panicIf(asset.stream.Play())
}

func panicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func initAssets(assets []SoundAsset) ([]SoundAsset, error) {
	for i, a := range assets {
		err := a.loadSound()
		if err != nil {
			return assets, err
		}
		assets[i] = a
	}
	return assets, nil
}
func createSoundAsset(soundFiles []string, box packr.Box) ([]SoundAsset, error) {
	s := make([]SoundAsset, len(soundFiles))
	for i, file := range soundFiles {
		f, err := box.Find(file)
		if err != nil {
			return s, err
		}
		s[i] = NewSoundAsset(f)
	}
	return initAssets(s)
}
