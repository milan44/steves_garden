//go:generate goversioninfo -icon=images/icon.ico
package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/jroimartin/gocui"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"os/user"
	"runtime"
	"runtime/debug"
	"strings"
	"sync"
	"time"
)

var inventoryView *gocui.View
var blockInfoView *gocui.View
var infoView *gocui.View
var gameView *gocui.View

var gui *gocui.Gui

var quitCalled = false

const version = "alpha v0.5.0"

var paused = false

func main() {
	_ = os.Setenv("GOTRACEBACK", "single")

	defer exitHandler()

	rand.Seed(time.Now().UTC().UnixNano())

	err := doWindowsCliSetup()
	if err != nil {
		log.Panicln(err)
	}

	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()
	g.InputEsc = true

	gui = g

	loadAllPlants()

	gameMapMutex = sync.Mutex{}
	bugMapMutex = sync.Mutex{}
	inventory, gameMap, bugMap, player, gameInfo = loadGame("")

	loadHarvesterFromGameInfo()

	g.SetManagerFunc(introLayout)

	intro()

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func crashLog(log string, typ string) (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", errors.New("failed to create crash report")
	} else {
		dir := strings.TrimRight(usr.HomeDir, "\\")
		dir = strings.TrimRight(dir, "/")

		_ = ioutil.WriteFile(dir+"/garden_"+typ+".log", []byte(log), 0777)

		return dir + "/garden_" + typ + ".log", nil
	}
}

var clearedScreen = false

func clearScreen() {
	if clearedScreen {
		return
	}
	clearedScreen = true
	if runtime.GOOS == "linux" || runtime.GOOS == "darwin" {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		_ = cmd.Run()
	} else if runtime.GOOS == "windows" {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		_ = cmd.Run()
	}
}
func exitHandler() {
	exitCode := 0
	shouldExit := false

	if r := recover(); r != nil {
		clearScreen()
		fmt.Println(localize["crash_rep"])

		file, err := crashLog(fmt.Sprint(r)+"\n"+string(debug.Stack()), "crash")
		if err != nil {
			fmt.Println(localize["crash_fail"])
		} else {
			fmt.Println(localize["crash_loc"] + " " + file)
		}
		exitCode = 1
		shouldExit = true
	}

	if quitCalled {
		clearScreen()
		shouldExit = true
		fmt.Println(localize["exit"])
		shouldIgnoreExitHandler = true
		err := saveGame(false, true)
		attempts := 1
		for attempts < 5 && err != nil {
			time.Sleep(100 * time.Millisecond)
			fmt.Println(localize["fail_save"] + "                                 ")

			err = saveGame(false, true)
			attempts++
		}

		if err != nil {
			fmt.Println(localize["fail_again"] + "                                                               ")

			fmt.Println(localize["creat_err"])

			file, err := crashLog(fmt.Sprint(err)+"\n"+string(debug.Stack()), "error")
			if err != nil {
				fmt.Println(localize["fail_err"])
			} else {
				fmt.Println(localize["err_loc"] + " " + file)
			}
		} else {
			fmt.Println(localize["save_succ"] + "                                     ")
		}
	}

	if shouldExit {
		fmt.Print(localize["press_entr"])
		_, _ = bufio.NewReader(os.Stdin).ReadBytes('\n')
		os.Exit(exitCode)
	}
}

func vPrintln(view *gocui.View, str string) {
	_, _ = fmt.Fprintln(view, str)
}
func vPrint(view *gocui.View, str string) {
	_, _ = fmt.Fprint(view, str)
}

const leftPanelWidth = 35

func getGameBorders() (int, int, int, int) {
	maxX, maxY := gui.Size()
	return leftPanelWidth + 1, 0, maxX - 1, maxY - 1
}
func getGameSize() (int, int) {
	maxX, maxY := gui.Size()
	return ((maxX - 1) - (leftPanelWidth + 1)) - 1, (maxY - 1) - 1
}

var layoutInitialized = false

func gameLoop() {
	go func() {
		defer exitHandler()

		for range time.Tick(1 * time.Second) {
			if layoutInitialized && !paused {
				isTicking = true
				tickGame()
				isTicking = false
			}
		}
	}()

	go func() {
		defer exitHandler()

		tick := 100 * time.Millisecond

		for {
			start := time.Now()
			if layoutInitialized && !paused {
				gui.Update(func(g *gocui.Gui) error {
					renderGame(false)
					return nil
				})
			}
			end := time.Now()

			wait := tick - end.Sub(start)
			if wait > 0 {
				time.Sleep(wait)
			}
		}
	}()
}

var isPopupOpen = false
var currentOpen string
var openedPopups = make(map[string]bool)

var popupViewMap = make(map[string]*gocui.View)

func popup(f func(*gocui.View, string, bool), name string, title string) error {
	if isPopupOpen && !paused {
		return nil
	}
	currentOpen = name
	isPopupOpen = true
	maxX, maxY := gui.Size()

	if openedPopups[name] {
		_ = gui.DeleteView(name)

		if v, err := gui.SetView(name, 0, 0, maxX-1, maxY-1); err != nil {
			if err != gocui.ErrUnknownView || v == nil {
				isPopupOpen = false
				paused = false
				currentOpen = ""
				return err
			}
			popupViewMap[name] = v

			openedPopups[name] = true

			v.Title = title + " (" + localize["esc_close"] + ")"

			f(v, name, false)
		}
	}

	if v, err := gui.SetView(name, 0, 0, maxX-1, maxY-1); err != nil {
		if err != gocui.ErrUnknownView || v == nil {
			isPopupOpen = false
			paused = false
			currentOpen = ""
			return err
		}
		popupViewMap[name] = v

		openedPopups[name] = true

		v.Title = title + " (" + localize["esc_close"] + ")"

		f(v, name, true)

		setPopupKey(name, gocui.KeyEsc, 0, func(index int, _ *gocui.View) {
			if currentOpen != name {
				return
			}

			_ = closePopup(name)
		})
	}
	return nil
}
func setPopupKey(name string, key gocui.Key, index int, f func(int, *gocui.View)) {
	if err := gui.SetKeybinding("", key, gocui.ModNone, func(g *gocui.Gui, view *gocui.View) error {
		if currentOpen != name {
			return nil
		}

		f(index, popupViewMap[name])

		return nil
	}); err != nil {
		log.Panicln(err)
	}
}
func closePopup(name string) error {
	_, _ = gui.SetViewOnBottom(name)
	isPopupOpen = false
	paused = false
	currentOpen = ""
	return nil
}

func layout(g *gocui.Gui) error {
	_, maxY := g.Size()
	if v, err := g.SetView("blockinfo", 0, 0, leftPanelWidth, 4); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		blockInfoView = v
		blockInfoView.Title = localize["block_info"]
	}
	if v, err := g.SetView("inventory", 0, 5, leftPanelWidth, maxY/2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		inventoryView = v
		inventoryView.Title = localize["inventory"]
	}
	if v, err := g.SetView("info", 0, maxY/2+1, leftPanelWidth, maxY-1); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		infoView = v
		infoView.Title = localize["info"]

		renderInfo()
	}
	x0, y0, x1, y1 := getGameBorders()
	if v, err := g.SetView("game", x0, y0, x1, y1); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}

		gameView = v
		gameView.Title = "Steve's Garden"
	}

	renderInventory()
	renderGame(false)

	layoutInitialized = true

	return nil
}

func quit(_ *gocui.Gui, _ *gocui.View) error {
	quitCalled = true
	for saving {
		time.Sleep(100 * time.Millisecond)
	}
	return gocui.ErrQuit
}

func chance(percent int) bool {
	return rand.Intn(100) < percent
}
