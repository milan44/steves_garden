package main

import (
	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
	"log"
	"time"
)

var flowerField = []string{
	"                   _                                         ",
	"                 _(_)_                          wWWWw   _    ",
	"     @@@@       (_)@(_)   vVVVv     _     @@@@  (___) _(_)_  ",
	"    @@()@@ wWWWw  (_)\\    (___)   _(_)_  @@()@@   Y  (_)@(_) ",
	"     @@@@  (___)     `|/    Y    (_)@(_)  @@@@   \\|/   (_)\\  ",
	"      /      Y       \\|    \\|/    /(_)    \\|      |/      |  ",
	"   \\ |     \\ |/       | / \\ | /  \\|/       |/    \\|      \\|/ ",
	"   \\\\|//   \\\\|///  \\\\\\|//\\\\\\|/// \\|///  \\\\\\|//  \\\\|//  \\\\\\|// ",
	"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
}
var stevesGardenTitle = []string{
	"████ █████ ████ █   █ ████ ████     ████  ██  ████ ███  ████ ██  █",
	"█      █   █    █   █ █    █        █    █  █ █  █ █  █ █    █ █ █",
	"████   █   ████ █   █ ████ ████     █    ████ ████ █  █ ████ █ █ █",
	"   █   █   █     █ █  █       █     █  █ █  █ █ █  █  █ █    █ █ █",
	"████   █   ████   █   ████ ████     ████ █  █ █  █ ███  ████ █  ██",
}

var skipIntro = false

func intro() {
	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}
	if err := gui.SetKeybinding("", gocui.KeyEnter, gocui.ModNone, func(g *gocui.Gui, v *gocui.View) error {
		skipIntro = true
		return nil
	}); err != nil {
		log.Panicln(err)
	}
}

func introLayout(g *gocui.Gui) error {
	maxX, maxY := g.Size()
	if v, err := g.SetView("intro_animation", -1, -1, maxX, maxY); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		go animateFlowerField(v)
	}

	return nil
}

var animationStarted = false

func animateFlowerField(view *gocui.View) {
	if animationStarted {
		return
	}
	animationStarted = true

	passedTime := 0 * time.Millisecond

	maxAnimationTime := 6 * time.Second
	delay := 200 * time.Millisecond
	longerField := make([]string, len(flowerField))
	for i, str := range flowerField {
		longerField[i] = str + str + str + str + str + str + str + str
	}
	character := len([]rune(flowerField[0])) - 1
	renderArray(view, longerField)

	for passedTime < maxAnimationTime {
		if skipIntro {
			break
		}
		time.Sleep(delay)
		longerField, character = shiftArray(longerField, flowerField, character)
		gui.Update(func(g *gocui.Gui) error {
			renderArray(view, longerField)
			return nil
		})

		passedTime += delay
	}

	gui.SetManagerFunc(mainmenuLayout)

	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	mainmenu()
}

func shiftArray(array []string, shiftArray []string, character int) ([]string, int) {
	for i, str := range array {
		array[i] = string([]rune(shiftArray[i])[character]) + str[:len(str)-1]
	}
	character--
	if character < 0 {
		character = len([]rune(shiftArray[0])) - 1
	}
	return array, character
}
func renderTitle(view *gocui.View, spaceTop int, extraSpace int) int {
	maxX, _ := gui.Size()
	paddingLeftTitle := pad(fl(maxX-strlen(stevesGardenTitle[0]), 2))
	halfPaddingTop := fl(spaceTop-(len(stevesGardenTitle)+extraSpace), 2)

	for i := 0; i < halfPaddingTop; i++ {
		vPrintln(view, "")
	}
	for _, str := range stevesGardenTitle {
		_, _ = color.New(color.FgGreen, color.Bold).Fprintln(view, paddingLeftTitle+str)
	}
	vPrintln(view, "")

	return halfPaddingTop
}
func renderArray(view *gocui.View, array []string) {
	maxX, maxY := gui.Size()

	subtitles := []string{
		version + " by twoot",
	}
	subtitleColors := []color.Attribute{
		color.FgCyan,
		color.FgYellow,
	}

	view.Clear()

	halfPaddingTop := renderTitle(view, maxY-len(array), len(subtitles))

	for i, str := range subtitles {
		paddingLeftSubTitle := pad(fl(maxX, 2) - fl(strlen(str), 2))
		_, _ = color.New(subtitleColors[i], color.Faint).Fprintln(view, paddingLeftSubTitle+str)
	}
	for i := 0; i < halfPaddingTop; i++ {
		vPrintln(view, "")
	}

	for _, str := range array {
		vPrintln(view, str)
	}
}

func strlen(str string) int {
	return len([]rune(str))
}

func pad(len int) string {
	str := ""
	for i := 0; i < len; i++ {
		str += " "
	}
	return str
}
