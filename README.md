![banner](images/banner.small.png)

#### [Trello Board](https://trello.com/b/hOi7yKAC/steves-garden)

### Download

[Windows Version](build/win/StevesGarden.exe)
[Linux Version (untested)](build/linux/StevesGarden)

**Other Operating systems**

You can build the game from source by downloading golang, then installing the dependencies and building it.

### Gameplay

Steve, your neighbour, noticed you were starting to get stressed out by life and needed a healthy outlet to find peace. He has asked you to check on his garden and you agreed.
Steve is planting weeds, cause he feels bad for the outcast plants and tries to give them a new home.


You can walk around the garden by using the arrow keys.

You can select different types of Sneeds in your inventory (press e) and plant them.
To plant Sneeds you have to first prepare the dirt (press p) and then plant the Sneed (press s).

The plants need time to grow, their growth status will appear in the block info if you walk on them.
They also need water within 2 Blocks of their position. To place water you dig a hole (press d) and dump the contents of your water bucket in it (press q).
After they are fully grown, you can harvest them (press h). You can sell harvested plants in Fishes Tremendous Trades (press o).
You can buy new Sneeds in Silly Bob's Seed Store (press b). Different plants have different sell prices.

You can quit or pause the game by pressing Escape. Quiting the game will pause the growth of all your plants.

**__Important: if you force quit the game, it can corrupt your save-file. Always quit the game by pressing Escape and selecting "Quit".__**

---

### Sidepanel

![sidepanel.png](images/sidepanel.png)

**Block Info**

Shows you inforation about the block you'Re standing on.

**Inventory**

Shows the contents of your inventory.

**Info**

Shows keybindings and the selected seed type.

## FAQ

### My garden is blank and i don't have any items in my inventory

This is most likely due to a crash and following that a corrupt save file. The game stores all data in your user folder.
Go to your user folder (e.g. `C:\Users\Username\`) in there should be a folder called `steves_garden_data`, delete its contents.
This will reset the game.