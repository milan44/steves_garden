package main

import (
	"os"
	"os/exec"
	"runtime"
)

func doWindowsCliSetup() error {
	if runtime.GOOS == "windows" {
		cmd := exec.Command("mode", "con:", "cols=130", "lines=38")
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		return cmd.Run()
	}
	return nil
}
