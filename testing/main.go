package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(string(unshift([]byte("n4s0@1_3qo3u_zBCLCFHAtm15q23q1023m3qBFAm43_0tm15q23q1BCLCFGAtm15q23q107BDFAtm15q23q108BJ"))))
	fmt.Println(string(shift([]byte("plant_a|200"))))
}

const rot = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_@|1234567890"
const rotSize = 12

func shift(b []byte) []byte {
	bs := []rune(string(b))
	bn := ""
	for _, by := range bs {
		in := strings.Index(rot, string(by))
		if in > -1 {
			in += rotSize
			if in >= len(rot) {
				in -= len(rot)
			}
			bn += string([]rune(rot)[in])
		} else {
			bn += string(by)
		}
	}
	return []byte(bn)
}
func unshift(b []byte) []byte {
	bs := []rune(string(b))
	bn := ""
	for _, by := range bs {
		in := strings.Index(rot, string(by))
		if in > -1 {
			in -= rotSize
			if in < 0 {
				in += len(rot)
			}
			bn += string([]rune(rot)[in])
		} else {
			bn += string(by)
		}
	}
	return []byte(bn)
}
